%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% University/School Laboratory Report
% LaTeX Template
% Version 3.1 (25/3/14)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% Original author:
% Linux and Unix Users Group at Virginia Tech Wiki
% (https://vtluug.org/wiki/Example_LaTeX_chem_lab_report)
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%   PACKAGES AND DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[babel]{csquotes}
\usepackage[T1]{fontenc}
\usepackage{hyperref}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{lmodern}
\usepackage{placeins}
\usepackage{selinput}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{amssymb}% http://ctan.org/pkg/amssymb
\usepackage{pifont}% http://ctan.org/pkg/pifont
%\usepackage{ulem}
\usepackage[usenames,dvipsnames]{xcolor}
%\usepackage[breakable, theorems, skins]{tcolorbox}
\usepackage{soul}
%\tcbset{enhanced}
\newcommand*\xor{\mathbin{\oplus}}
\newcommand{\shellcmd}[1]{\\\indent\indent\texttt{\footnotesize\ #1}\\}
\newcommand{\shellcmdinline}[1]{\texttt{\footnotesize #1}}
\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}

\lstset{ %
  backgroundcolor=\color{white},   % choose the background color
  basicstyle=\footnotesize,        % size of fonts used for the code
  breaklines=true,                 % automatic line breaking only at whitespace
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  keywordstyle=\color{blue},       % keyword style
  stringstyle=\color{mymauve},      % string literal style
  language=C,
  numbers=left
}
%\usepackage{times} % Uncomment to use the Times New Roman font

%----------------------------------------------------------------------------------------
%   DOCUMENT INFORMATION
%----------------------------------------------------------------------------------------

\title{\huge{Exercise 1} \\\large{Summer Term 2015/16} \\\quad \\\huge{GNU Compiler}}

\author{Clemens \textsc{Brunner}\\
        Michael \textsc{Fröwis} \\
}





\date{\today} % Date for the report

\begin{document}

\maketitle % Insert the title, author and date
\thispagestyle{empty}
\newpage
\thispagestyle{empty}

\newpage

\section{Introduction}
This paper provides a very short overview of the compilation process of gcc and its overall components, including data structures. Section \ref{components} describes the overall workflow of gcc and sketches what data structures are used. In Section \ref{internal} the most important intermediate representations (IRs) of gcc are described. Section \ref{compilation} briefly sketches the other steps included in the compilation process besides the actual compilation.

\section{Components \cite{redhat}}
\label{components}

Figure \ref{fig:gcc_phases} shows the different compilation phases of gcc. In the front/middle end the source code of all supported programming languages are transformed into a special tree representation(AST) for each language. Figure \ref{fig:Ast} shows such a representation for the code \lstinline{x = 3 * y - z}. The AST is then transformed by the so called "expander" into the RTL (Register Transfer Language) representation. RTL is a platform independent "assembly-like language" with infinite register memory. This abstract language is used to optimize the source code. After the optimization step the machine code (assembly) is generated out of the RTL representation.

\begin{figure}[ht]
\centering
\includegraphics[width=\textwidth]{gcc-phases.png}
\caption{GCC Phases \cite{redhat}}
\label{fig:gcc_phases}
\end{figure}


\begin{figure}[ht]
\centering
\includegraphics[width=5cm]{ast.png}
\caption{Abstract syntax tree \cite{redhat}}
\label{fig:Ast}
\end{figure}

One problem with this internal organization was that the optimization took place only after the RTL transformation. The RTL representation is pretty low level and makes higher-level optimization's and analysis hard. With gcc version 4.0 Red Hat introduced a new tree like IR to eases some optimization's as we will describe in the next section.

\subsection{Important Internal Structures - IR}
\label{internal}
\begin{figure}[ht]
\centering
\includegraphics[width=8cm]{tree-ssa-phases.png}
\caption{Tree SSA Phases \cite{redhat}}
\label{fig:tree-ssa-phases}
\end{figure}

Figure \ref{fig:tree-ssa-phases} shows the new internal structure of the gcc compilation process. 
\subsubsection{Abstract Syntax Trees}
To generate machine code out of the source code \lstinline{x = 3 * y - z} gcc first parses (lexing then parsing) the source language to generate an abstract syntax tree \ref{fig:Ast}. This tree can be different for each programming language.
\subsubsection{GENERIC}
GENERIC is a super-set of all language specific syntax trees. The GENERIC representation is independent of the language, thus GENERIC. This means that the transformation to GENERIC removes all language specific statements.

\begin{lstlisting}[caption=GENERIC: {\url{https://www.redhat.com/magazine/002dec04/features/gcc/} , visited 2016-03-16.}, label=generic]
if (foo (a + b, c))
  c = b++ / a
endif
return c
\end{lstlisting}
\subsubsection{GIMPLE}
 GIMPLE is as GENERIC a tree representation but it has some restrictions on statements i.e. no statement has more then 3 operands, a if statement has only a single comparison.
\begin{lstlisting}[caption=GIMPLE: {\url{https://www.redhat.com/magazine/002dec04/features/gcc/} , visited 2016-03-16.}, label=gimple]
t1 = a + b
t2 = foo (t1, c)
if (t2 != 0)
  t3 = b
  b = b + 1
  c = t3 / a
endif
return c
\end{lstlisting}

\subsubsection{Tree SSA Optimizer}
The static single assignment (SSA) is a representation usually generated from some GIMPLE input. The idea behind SSA is that each variable is assigned only once. If a variable $x$ is assigned more than once it will be replaced with i.e. $x_1, \dots, x_n$. This means that there is some kind of versioning of variable assignments. If the variable is used again in the program than the compiler uses the newest version of the variable. The reason for this is that if everything is fixed, the compiler knows that it does not need to calculate these steps again, see listing \ref{nocalc}. If the new assignment of $x$ is linked to a condition, than the compiler stores both versions and knows that it has to calculate the value on runtime, see listing \ref{calc}.

\begin{lstlisting}[caption=No need to recomputate, label=nocalc]
x = 10
y = 12
z = x + y
out = x * y \\ out is always = 264
\end{lstlisting}


\begin{lstlisting}[caption=Need to computate on runtime, label=calc]
x = 10
y = 12
if (time() mod 2 == 0)
  y = 0
out = x * y \\ out can be 120 or 0
\end{lstlisting}

\section{Compilation Process++ \cite{gccmaketut}}
\label{compilation}
Besides the actual compilation process (source language to assembler) gcc includes a couple of other steps that are needed to produce an executable program. As figure \ref{fig:gcc_compilation_process} suggests this includes preprocessing, assembling and linking. Preprocessing expands for example macro definitions and includes header files before the actual compilation. After compilation the Assembler converts the platform specific assembly code to actual machine code. And finally the linker is combines all connected object files to one single executable. 
\begin{figure}[ht]
\centering
\includegraphics[width=9cm]{GCC_CompilationProcess.png}
\caption{GCC Compilation Process \cite{gccmaketut}}
\label{fig:gcc_compilation_process}
\end{figure}


\FloatBarrier


\bibliography{biblio}{}
\bibliographystyle{alpha}

\end{document}

