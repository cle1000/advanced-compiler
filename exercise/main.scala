import scala.io._
import scala.util._
import at.mCc.compiler._

object CompilerCLI {
  def main(args: Array[String]) = {
  	val fileToParse = args(0)
  	println("--Compiling " + fileToParse)
  	mCCompiler.Compile(Source.fromFile(fileToParse).mkString)
    match {		
      case Success(x) =>  println(x)
  		case Failure(x) =>  println(x.getMessage)
  	}
  	
  	println("--Done Compiling " + fileToParse)
  } 
}
