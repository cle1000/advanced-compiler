lazy val root = (project in file(".")).
  settings(
    name := "mCc",
    version := "1.0"
   	//, scalaVersion := "2.11.7"
  )

libraryDependencies ++= Seq(
  "org.parboiled" %% "parboiled" % "2.1.2",
  "com.github.nikita-volkov" % "sext" % "0.2.4",
  "org.scalactic" %% "scalactic" % "2.2.6",
  "org.scalatest" %% "scalatest" % "2.2.6" % "test"
)

concurrentRestrictions in Global += Tags.limit(Tags.Test, 1)

//baseDirectory in test := file(".")
