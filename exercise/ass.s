.globl  main
.text
main:
push %ebp
mov %esp, %ebp
sub $116, %esp
mov $0, %eax
mov %eax, -4(%ebp)
mov $1, %eax
mov %eax, -8(%ebp)
mov $2, %eax
mov %eax, -12(%ebp)
mov $3, %eax
mov %eax, -16(%ebp)
mov -4(%ebp), %eax
add -8(%ebp), %eax
mov %eax, -20(%ebp)
mov -20(%ebp), %eax
mov %eax, -24(%ebp)
mov -4(%ebp), %eax
add -8(%ebp), %eax
mov %eax, -28(%ebp)
mov -28(%ebp), %eax
mov %eax, -32(%ebp)
mov -4(%ebp), %eax
mov $0, %ebx
cmp %ebx, %eax
je _t16
mov -16(%ebp), %eax
add -12(%ebp), %eax
mov %eax, -36(%ebp)
mov -36(%ebp), %eax
mov %eax, -40(%ebp)
jmp _t17
_t16:
mov -4(%ebp), %eax
add -8(%ebp), %eax
mov %eax, -44(%ebp)
mov -44(%ebp), %eax
mov %eax, -48(%ebp)
mov -52(%ebp), %eax
add -12(%ebp), %eax
mov %eax, -56(%ebp)
mov -56(%ebp), %eax
mov %eax, -60(%ebp)
mov -48(%ebp), %eax
mov $0, %ebx
cmp %ebx, %eax
je _t10
mov -8(%ebp), %eax
add $8, %eax
mov %eax, -64(%ebp)
mov -64(%ebp), %eax
mov %eax, -16(%ebp)
mov -52(%ebp), %eax
add -12(%ebp), %eax
mov %eax, -68(%ebp)
mov -68(%ebp), %eax
mov %eax, -72(%ebp)
mov -16(%ebp), %eax
add -76(%ebp), %eax
mov %eax, -80(%ebp)
mov -80(%ebp), %eax
mov %eax, -84(%ebp)
jmp _t11
_t10:
mov -8(%ebp), %eax
add $5, %eax
mov %eax, -88(%ebp)
mov -88(%ebp), %eax
mov %eax, -16(%ebp)
mov -4(%ebp), %eax
add -8(%ebp), %eax
mov %eax, -92(%ebp)
mov -92(%ebp), %eax
mov %eax, -96(%ebp)
mov -16(%ebp), %eax
add -76(%ebp), %eax
mov %eax, -100(%ebp)
mov -100(%ebp), %eax
mov %eax, -84(%ebp)
_t11:
mov -4(%ebp), %eax
add -8(%ebp), %eax
mov %eax, -104(%ebp)
mov -104(%ebp), %eax
mov %eax, -108(%ebp)
_t17:
mov -112(%ebp), %eax
imul $2, %eax
mov %eax, -116(%ebp)
mov -116(%ebp), %eax
mov %eax, -112(%ebp)
mov -112(%ebp), %eax
add $116, %esp
pop %ebp
ret
