import org.scalatest._
import Matchers._
import at.mCc.ast.optimisations._
import at.mCc.ast._
import scala.util._

class LoopSpec extends FlatSpec with Matchers {

    "example" should "should produce correct classification" in {
        CI.FromAST((h.ast("""for(int i = 0; i < size; i=i+1){
                            for(int j = 0; j < size; j=j+1){    
                                int a = 10; 
                                int sum = c[j]; 
                                int a = j + g[i];   
                            }
                           }""")) match { 
                                    case Success(x) => x
                                    case Failure(e) => throw e
                                }
                            ) should equal (
                            
                                List(
                                    (Declaration(INT(),"j",Some(Lit("0",INT()))), Set()), 
                                    (Declaration(INT(),"a",Some(Lit("10",INT()))), Set()), 
                                    (Declaration(INT(),"sum",Some(Var("c",Some(Var("j",None))))), Set("j")),
                                    (Declaration(INT(),"a", Some(Bin_Expr(Var("j",None),PLUS(),Var("g",Some(Var("i",None)))))), Set("i", "j")),
                                    (Bin_Expr(Var("j",None),ASS(),Bin_Expr(Var("j",None),PLUS(),Lit("1",INT()))), Set("j")), 
                                    (Bin_Expr(Var("i",None),ASS(),Bin_Expr(Var("i",None),PLUS(),Lit("1",INT()))),Set("i"))
                                )
                            )
    }
}

