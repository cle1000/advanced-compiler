import org.scalatest._
import Matchers._
import at.mCc.parser._
import scala.util._
import at.mCc.ast._

class AstSpec extends FlatSpec with Matchers {

	"parse bin operation expression" should "produce correct ast" in {
    	h.ast("1+2;") should equal (Success(Compound(Vector(
														Bin_Expr(Lit("1",INT()),PLUS(),Lit("2",INT()))
													))))
    }

    "assign expression" should "produce correct ast" in {
		h.ast("FooBaarr=4.0;") should equal (Success(Compound(Vector(
    													Bin_Expr(Var("FooBaarr",None), ASS(), Lit("4.0",FLOAT()))
    												))))
	}

	"declaration expression" should "produce correct ast" in {
		h.ast("int x=4;") should equal (Success(Compound(Vector(
														Declaration(INT(), "x", Some(Lit("4",INT())))
													))))
	}

	"parse declaration without assignment" should "produce correct ast" in {
    	h.ast("int x;") should equal (Success(Compound(Vector(
														Declaration(INT(), "x", None)
													))))
    }

    "parse if" should "produce correct ast" in {
    	h.ast("if(1==1) x=2; else y=3;") should equal (Success(Compound(Vector(
														If( 
															Bin_Expr(Lit("1",INT()),EQ(),Lit("1",INT())),
															Bin_Expr(Var("x",None),ASS(),Lit("2",INT())),
															Some(
																Bin_Expr(Var("y",None),ASS(),Lit("3",INT()))
															)
														)
													))))
    }

    "parse if without else" should "produce correct ast" in {
    	h.ast("if(1==1) x=2;") should equal (Success(Compound(Vector(
													If( 
														Bin_Expr(Lit("1",INT()),EQ(),Lit("1",INT())),
														Bin_Expr(Var("x",None),ASS(),Lit("2",INT())),
														None
													)
												))))
    }

    "parse if with paren" should "produce correct ast" in {
    	h.ast("if(1==1){x=2;}else y=3;") should equal (Success(Compound(Vector(
													If( 
														Bin_Expr(Lit("1",INT()),EQ(),Lit("1",INT())),
														Compound(Vector(Bin_Expr(Var("x",None),ASS(),Lit("2",INT())))),
														Some(
															Bin_Expr(Var("y",None),ASS(),Lit("3",INT()))
														)
													)
												))))
    }

    "parse with scope variables" should "produce correct ast" in {
		h.ast("{int x = 0; x = 3;	{ int x = 4; x = 5; { int x = 3; x = x * x; } x = 6;}	int y = x;}") should equal (Success(Compound(Vector(
															Compound(Vector(
																Declaration(INT(),"x",Some(Lit("0",INT()))),
																Bin_Expr(Var("x",None),ASS(),Lit("3",INT())),
																Compound(Vector(
																	Declaration(INT(),"s_9_10_11_x",Some(Lit("4",INT()))),
																	Bin_Expr(Var("s_9_10_11_x",None),ASS(),Lit("5",INT())),
																	Compound(Vector(
																		Declaration(INT(),"s_9_10_11_12_x",Some(Lit("3",INT()))),
																		Bin_Expr(Var("s_9_10_11_12_x",None),ASS(),
																			Bin_Expr(Var("s_9_10_11_12_x",None),MUL(),Var("s_9_10_11_12_x",None))))),
																	Bin_Expr(Var("s_9_10_11_x",None),ASS(),Lit("6",INT())))),
																Declaration(INT(),"y",Some(Var("x",None)))))))))
	}

	"parse while" should "produce correct ast" in {
		h.ast("int z = 2; while(!(z > 1000)){ z = z * z;}") should equal (
			Success(
				Compound(
					Vector(
						Declaration(INT(),"z",Some(Lit("2",INT()))),
						While(Un_Expr(NOT(),Bin_Expr(Var("z",None),GT(),Lit("1000",INT()))),
							Compound(Vector(Bin_Expr(Var("z",None),ASS(),Bin_Expr(Var("z",None),MUL(),Var("z",None))))),
							None
							))))

		)
	}

	"parse for" should "produce correct ast" in {
		h.ast("int z = 1; for(int i = 1; i < 5; i=i+1){ z = z * i;}") should equal (
			Success(
				Compound(Vector(
						Declaration(INT(),"z",Some(Lit("1",INT()))),
						Compound(Vector(Declaration(INT(),"i",Some(Lit("1",INT()))),
						While(Bin_Expr(Var("i",None),LT(),Lit("5",INT())),
							Compound(Vector(Compound(Vector(
								Bin_Expr(Var("z",None),ASS(),Bin_Expr(Var("z",None),MUL(),Var("i",None))))),
								Bin_Expr(Var("i",None),ASS(),Bin_Expr(Var("i",None),PLUS(),Lit("1",INT()))))),
							Some(Var("i",None))
							))))))
		)
	}

	"func and call" should "produce correct ast" in {
		AST("int print(int a){ return a; } int main(){int a = 3; return print(a); }") should equal (Success(
				Compound(Vector(
					Fun_Def(INT(),"print",Vector(("a",INT())),
						Compound(Vector(Return(Var("a",None))))),
					Fun_Def(INT(),"main",Vector(),
						Compound(Vector(
							Declaration(INT(),"a",Some(Lit("3",INT()))),
							Return(Fun_Call("print",Vector(Var("a",None)))))))))))
	}

	"Array" should "produce correct ast" in {
    	h.ast("int i = 1; int a[5]; a[0] = 1; a[i+1] = 2;") should equal (Success(Compound(
    			Vector(
    				Declaration(INT(),"i",Some(Lit("1",INT()))),
    				Arr_Declaration(INT(),"a",5),
    				Bin_Expr(Var("a",Some(Lit("0",INT()))),ASS(),Lit("1",INT())), Bin_Expr(Var("a",Some(Bin_Expr(Var("i",None),PLUS(),Lit("1",INT())))),ASS(),Lit("2",INT()))))))
  	}
}