import scala.util._
import at.mCc.parser._
import at.mCc.ast._
import at.mCc.tac.types._
import at.mCc.tac._
import at.mCc.assembly._



package object h {
	def within_main(x:String) = "int main(){" + x + "}"
	def extract_main(x: Try[Seq[Fun_Definition]]) = x match {
		case Success(Vector(Fun_Definition("int","main",Vector(),Comp_Statement(body)))) => Success(body)
		case x => x
	}

	def extract_main_ast(x: Try[AST]) = x match {
		case Success(Compound(Vector(Fun_Def(INT(), "main", Vector(), body)))) => Success(body)
		case x => x
	}

	def extract_main_tac(x: Try[TAC_Program]) = x match {
		case Success(Label(main) :: BeginFunc(x) :: xs) => Success(xs.dropRight(1))
		case x => x
	}
	
	def parse(x:String) = h.extract_main(mCParser(h.within_main(x)))

	def ast(x: String) = h.extract_main_ast(AST(h.within_main(x)))

	def tac(x: String) = h.extract_main_tac(TAC(h.within_main(x)))

	def ass(x: String) = ASSEMBLY(h.within_main(x))
}
