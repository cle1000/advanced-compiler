import org.scalatest._
import Matchers._
import at.mCc.assembly._
import at.mCc.tac.optimisations._
import at.mCc.parser._
import scala.util._
import at.mCc.ast._
import scala.io._
import at.mCc.compiler._


class AssSpec extends FlatSpec with Matchers {

    "parse bin operation expression" should "produce correct assembly" in {
        h.ass("1+2;") should equal (Success(List(".globl  main",
                                                 ".text",
                                                 "main:",
                                                 "push %ebp",
                                                 "mov %esp, %ebp",
                                                 "sub $4, %esp",
                                                 "mov $1, %eax",
                                                 "add $2, %eax",
                                                 "mov %eax, -4(%ebp)",
                                                 "add $4, %esp",
                                                 "pop %ebp",
                                                 "ret")))
    }

    "assign expression" should "produce correct assembly" in {
        h.ass("FooBaarr=4.0;") should equal (Success(List(".globl  main",
                                                          ".text",
                                                          "main:",
                                                          "push %ebp",
                                                          "mov %esp, %ebp",
                                                          "sub $4, %esp",
                                                          "mov $4.0, %eax",
                                                          "mov %eax, -4(%ebp)",
                                                           "add $4, %esp",
                                                           "pop %ebp",
                                                           "ret")))
    }

    "declaration expression" should "produce correct assembly" in {
        h.ass("int x=4;") should equal (Success(List(".globl  main",
                                                     ".text",
                                                     "main:",
                                                     "push %ebp",
                                                     "mov %esp, %ebp",
                                                     "sub $4, %esp",
                                                     "mov $4, %eax",
                                                     "mov %eax, -4(%ebp)",
                                                     "add $4, %esp",
                                                     "pop %ebp",
                                                     "ret")))
    }

    "parse declaration without assignment" should "does not produce assembly code" in {
        h.ass("int x;") should equal (Success(List(".globl  main",
                                           ".text",
                                           "main:",
                                           "push %ebp",
                                           "mov %esp, %ebp",
                                           "sub $0, %esp",
                                           "add $0, %esp",
                                           "pop %ebp",
                                           "ret")))
    }

    "parse if" should "produce correct assembly" in {
        h.ass("if(1==1) x=2; else y=3;") should equal (Success(List(".globl  main", 
                                                                    ".text",
                                                                    "main:",
                                                                    "push %ebp",
                                                                    "mov %esp, %ebp",
                                                                    "sub $8, %esp",
                                                                    "mov $1, %eax",
                                                                    "mov $1, %ebx",
                                                                    "cmp %ebx, %eax",
                                                                    "je _t2",
                                                                    "mov $3, %eax",
                                                                    "mov %eax, -4(%ebp)",
                                                                    "jmp _t3",
                                                                    "_t2:",
                                                                    "mov $2, %eax",
                                                                    "mov %eax, -8(%ebp)",
                                                                    "_t3:",
                                                                    "add $8, %esp",
                                                                    "pop %ebp",
                                                                    "ret")))
    }

    "parse if without else" should "produce correct tac" in {
        h.ass("if(1==1) x=2;") should equal (Success(List(".globl  main",
                                                          ".text",
                                                          "main:",
                                                          "push %ebp",
                                                          "mov %esp, %ebp",
                                                          "sub $4, %esp",
                                                          "mov $1, %eax",
                                                          "mov $1, %ebx",
                                                          "cmp %ebx, %eax",
                                                          "je _t2",
                                                          "jmp _t3",
                                                          "_t2:",
                                                          "mov $2, %eax",
                                                          "mov %eax, -4(%ebp)",
                                                          "_t3:",
                                                          "add $4, %esp",
                                                          "pop %ebp",
                                                          "ret")))
    }

    "parse if with paren" should "produce same tac as without" in {
        h.ass("if(1==1){x=2;}else y=3;") should equal (h.ass("if(1==1) x=2; else y=3;"))
    }

    "parse for" should "produce correct tac" in {
        h.ass("int z = 1; for(int i = 1; i < 5; i=i+1){ z = z * i;}") should equal (Success(List(".globl  main", 
                                                                                         ".text",
                                                                                         "main:",
                                                                                         "push %ebp",
                                                                                         "mov %esp, %ebp",
                                                                                         "sub $16, %esp",
                                                                                         "mov $1, %eax",
                                                                                         "mov %eax, -4(%ebp)",
                                                                                         "mov $1, %eax",
                                                                                         "mov %eax, -8(%ebp)",
                                                                                         "_t3:",
                                                                                         "mov -8(%ebp), %eax",
                                                                                         "mov $5, %ebx",
                                                                                         "cmp %ebx, %eax",
                                                                                         "jl _t4",
                                                                                         "jmp _t5",
                                                                                         "_t4:",
                                                                                         "mov -4(%ebp), %eax",
                                                                                         "imul -8(%ebp), %eax",
                                                                                         "mov %eax, -12(%ebp)",
                                                                                         "mov -12(%ebp), %eax",
                                                                                         "mov %eax, -4(%ebp)",
                                                                                         "mov -8(%ebp), %eax",
                                                                                         "add $1, %eax",
                                                                                         "mov %eax, -16(%ebp)",
                                                                                         "mov -16(%ebp), %eax",
                                                                                         "mov %eax, -8(%ebp)",
                                                                                         "jmp _t3",
                                                                                         "_t5:",
                                                                                         "add $16, %esp",
                                                                                         "pop %ebp",
                                                                                         "ret")))
    }

    "parse while" should "produce correct tac" in {
        h.ass("int z = 2; while(z < 1000){ z = z * z;}") should equal (Success(List(".globl  main",
                                                                                   ".text",
                                                                                   "main:",
                                                                                   "push %ebp",
                                                                                   "mov %esp, %ebp",
                                                                                   "sub $8, %esp",
                                                                                   "mov $2, %eax",
                                                                                   "mov %eax, -4(%ebp)",
                                                                                   "_t2:",
                                                                                   "mov -4(%ebp), %eax",
                                                                                   "mov $1000, %ebx",
                                                                                   "cmp %ebx, %eax",
                                                                                   "jl _t3",
                                                                                   "jmp _t4",
                                                                                   "_t3:",
                                                                                   "mov -4(%ebp), %eax",
                                                                                   "imul -4(%ebp), %eax",
                                                                                   "mov %eax, -8(%ebp)",
                                                                                   "mov -8(%ebp), %eax",
                                                                                   "mov %eax, -4(%ebp)",
                                                                                   "jmp _t2",
                                                                                   "_t4:",
                                                                                   "add $8, %esp",
                                                                                   "pop %ebp",
                                                                                   "ret")))
    
    }
    "func and call" should "produce correct ass" in {
        ASSEMBLY("int print(int a, float b){ return a; } int main(){int a = 3; return print(a, 4.0); }") should equal (Success(List(".globl  main",
                                                                                                                                    ".text",
                                                                                                                                    "print:",
                                                                                                                                    "push %ebp",
                                                                                                                                    "mov %esp, %ebp",
                                                                                                                                    "sub $4, %esp", 
                                                                                                                                    "mov 8(%ebp), %eax",
                                                                                                                                    "mov %eax, -4(%ebp)",
                                                                                                                                    "mov 12(%ebp), %eax",
                                                                                                                                    "mov %eax, -8(%ebp)",
                                                                                                                                    "mov -8(%ebp), %eax",
                                                                                                                                    "add $4, %esp",
                                                                                                                                    "pop %ebp",
                                                                                                                                    "ret",
                                                                                                                                    "main:",
                                                                                                                                    "push %ebp",
                                                                                                                                    "mov %esp, %ebp",
                                                                                                                                    "sub $4, %esp",
                                                                                                                                    "mov $3, %eax",
                                                                                                                                    "mov %eax, -4(%ebp)",
                                                                                                                                    "push -4(%ebp)",
                                                                                                                                    "push $4.0",
                                                                                                                                    "call print",
                                                                                                                                    "add $8, %esp",
                                                                                                                                    "mov %eax, %eax",
                                                                                                                                    "add $4, %esp",
                                                                                                                                    "pop %ebp",
                                                                                                                                    "ret")))
    }

    "Array" should "produce correct ass" in {
        ASSEMBLY("int main(){int i = 1; int a[5]; a[0] = 1; a[i+1] = 2; print_int(a[i+1]);}") should equal (Success(List(".globl  main",
                                                                                                                         ".text",
                                                                                                                         "main:",
                                                                                                                         "push %ebp",
                                                                                                                         "mov %esp, %ebp",
                                                                                                                         "sub $32, %esp",
                                                                                                                         "mov $1, %eax",
                                                                                                                         "mov %eax, -4(%ebp)",
                                                                                                                         "mov $0, %esi",
                                                                                                                         "neg %esi",
                                                                                                                         "mov $1, %eax",
                                                                                                                         "mov %eax, -8(%ebp,%esi,4)",
                                                                                                                         "mov -4(%ebp), %eax",
                                                                                                                         "add $1, %eax",
                                                                                                                         "mov %eax, -28(%ebp)",
                                                                                                                         "mov -28(%ebp), %esi",
                                                                                                                         "neg %esi",
                                                                                                                         "mov $2, %eax",
                                                                                                                         "mov %eax, -8(%ebp,%esi,4)",
                                                                                                                         "mov -4(%ebp), %eax",
                                                                                                                         "add $1, %eax",
                                                                                                                         "mov %eax, -32(%ebp)",
                                                                                                                         "mov -32(%ebp), %esi",
                                                                                                                         "neg %esi",
                                                                                                                         "push -8(%ebp,%esi,4)",
                                                                                                                         "call print_int",
                                                                                                                         "add $4, %esp",
                                                                                                                         "add $32, %esp",
                                                                                                                         "pop %ebp",
                                                                                                                         "ret")))
    }
}
