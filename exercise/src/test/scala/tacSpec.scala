import org.scalatest._
import Matchers._
import at.mCc.tac._
import at.mCc.tac.optimisations._
import at.mCc.parser._
import scala.util._
import at.mCc.ast._
import scala.io._
import at.mCc.compiler._


class TacSpec extends FlatSpec with Matchers {

    "parse bin operation expression" should "produce correct tac" in {
        h.tac("1+2;") should equal (Success(List(
                                                    Bin_Op("1",PLUS(),"2","_t1")
                                                )))
    }

    "assign expression" should "produce correct tac" in {
        h.tac("FooBaarr=4.0;") should equal (Success(List(
                                                        Copy("$FooBaarr","4.0")
                                                        )))
    }

    "declaration expression" should "produce correct tac" in {
        h.tac("int x=4;") should equal (Success(List(
                                                    Copy("$x","4")
                                                )))
    }

    "parse declaration without assignment" should "does not produce tac code" in {
        h.tac("int x;") should equal (Success(List(
                                                                                                    
                                                )))
    }

    "parse if" should "produce correct tac" in {
        h.tac("if(1==1) x=2; else y=3;") should equal (Success(List(
                                                            Cond_Jump("1",EQ(),"1",Label("_t2")),
                                                            Copy("$y", "3"),
                                                            Jump(Label("_t3")),
                                                            Label("_t2"),
                                                            Copy("$x", "2"),
                                                            Label("_t3")

                                                            )))
    }

    "parse if without else" should "produce correct tac" in {
        h.tac("if(1==1) x=2;") should equal (Success(List(
                                                        Cond_Jump("1",EQ(),"1",Label("_t2")),
                                                        Jump(Label("_t3")),
                                                        Label("_t2"),
                                                        Copy("$x", "2"),
                                                        Label("_t3")
                                                    )))
    }

    "parse if with paren" should "produce same tac as without" in {
        h.tac("if(1==1){x=2;}else y=3;") should equal (h.tac("if(1==1) x=2; else y=3;"))
    }

    "parse for" should "produce correct tac" in {
        h.tac("int z = 1; for(int i = 1; i < 5; i=i+1){ z = z * i;}") should equal (
                 Success(List(
                            Copy("$z","1"), 
                            Copy("$i","1"), 
                            Label("_t3"), 
                            Cond_Jump("$i",LT(),"5",Label("_t4")), 
                            Jump(Label("_t5")), 
                            Label("_t4"), 
                            Bin_Op("$z",MUL(),"$i","_t1"), 
                            Copy("$z","_t1"), 
                            Bin_Op("$i",PLUS(),"1","_t2"), 
                            Copy("$i","_t2"), 
                            Jump(Label("_t3")), 
                            Label("_t5")
                        ))
            )
    }

    "parse while" should "produce correct tac" in {
        h.tac("int z = 2; while(z < 1000){ z = z * z;}") should equal (
                    Success(
                        List(
                            Copy("$z","2"), 
                            Label("_t2"), 
                            Cond_Jump("$z",LT(),"1000",Label("_t3")), 
                            Jump(Label("_t4")), 
                            Label("_t3"), 
                            Bin_Op("$z",MUL(),"$z","_t1"), 
                            Copy("$z","_t1"), 
                            Jump(Label("_t2")), 
                            Label("_t4")))
                    )
    }
    "func and call" should "produce correct tac" in {
        TAC("int print(int a, float b){ return a; } int main(){int a = 3; return print(a, 4.0); }") should equal (Success(
                    List(
                        Label("print"),
                        BeginFunc(1),
                        Pop("$b",1),
                        Pop("$a",2),
                        Ret("$a"),
                        EndFunc(1),
                        Label("main"),
                        BeginFunc(1),
                        Copy("$a","3"),
                        Push("$a"),
                        Push("4.0"),
                        Call(Label("print")),
                        CleanFuncCall(2),
                        Ret("return"),
                        EndFunc(1))))
    }

    "extract func from tac" should "produce correct tac" in {
        TAC("int print(int a, float b){ return a; } int main(){int a = 3; return print(a, 4.0); }").map(TAC.GetFunction("main")) should equal (Success(
                    List(
                        Copy("$a","3"),
                        Push("$a"),
                        Push("4.0"),
                        Call(Label("print")),
                        CleanFuncCall(2),
                        Ret("return")
                        )))
    }

    "Array" should "produce correct tac" in {
        TAC("int main(){int i = 1; int a[5]; a[0] = 1; a[i+1] = 2;}") should equal (Success(List(
                                                                                        Label("main"),
                                                                                        BeginFunc(7),
                                                                                        Copy("$i","1"),
                                                                                        StackAlloc("a",5),
                                                                                        Copy("%a-0","1"),
                                                                                        Bin_Op("$i",PLUS(),"1","_t1"),
                                                                                        Copy("%a-_t1","2"),
                                                                                        EndFunc(7))))
    }
}
