import org.scalatest._
import Matchers._
import at.mCc.tac.optimisations._
import at.mCc.tac._
import at.mCc.ast._
import scala.util._

class BbSpec extends FlatSpec with Matchers {

	"example" should "have 4 basic blocks" in {
        BB.FromTac(List(
                                Cond_Jump("1",EQ(),"1",Label("_t2")),
                                Jump(Label("_t3")),
                                Label("_t2"),
                                Copy("$x", "2"),
                                Label("_t3")
                            ))  should have size 4
    }

    "5 jumps and two consecutive labels" should "create 6 basic blocks" in {
        BB.FromTac(List( 
                                Cond_Jump("1",EQ(),"1",Label("_t2")),
                                Jump(Label("_t3")),
                                Label("_t2"),
                                Label("_t3"),
                                Jump(Label("_t3")),
                                Jump(Label("_t3")),
                                Cond_Jump("1",EQ(),"1",Label("_t2"))
                            ))  should have size 6
    }

    "only labels" should "not create basic blocks" in {
        BB.FromTac(List(
                                Label("_t2"),
                                Label("_t3")
                            ))  should have size 2
    }



    "tac with 3 blocks " should " have 3 basic blocks" in {
        BB.FromTac(List(
                                Cond_Jump("1",EQ(),"1",Label("_t2")),
                                Jump(Label("_t3")),
                                Label("_t2"),
                                Copy("$x", "2"),
                                Label("_t3"),
                                Copy("$y", "6")
                            ))  should have size 4
    }


    "two labels with code" should " create two basic blocks" in {
        BB.FromTac(List(
                                Label("_t2"),
                                Copy("$y", "6"),
                                Label("_t3"),
                                Copy("$y", "6")
                            ))  should have size 2
    }

}