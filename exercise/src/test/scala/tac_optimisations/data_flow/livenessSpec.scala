import org.scalatest._
import Matchers._
import at.mCc.tac.optimisations._
import at.mCc.tac.optimisations.types._
import at.mCc.tac.optimisations.dataflow._
import at.mCc.tac._
import at.mCc.ast._
import scala.util._
import scala.io._
import scala.collection.mutable.Set
import at.mCc.dot._

class LivenessSpec extends FlatSpec with Matchers {

	val input = Map(
		        	0 -> Node(0,
        						List(
        							Copy("i","1")),
        						Set(1),Set(),Set()),
        	        1 -> Node(1,
        						List(
        							Label("xxxx"),
        							Copy("a","9"),
        							Copy("c","10"),
        							Cond_Jump("1",EQ(),"1",Label("_t16"))),
        						Set(2, 3),Set(0,7),Set()),
        			2 -> Node(2,
        					List(
        						Copy("b","1"),
        						Copy("c","2"),
        						Copy("d","3"),
        						Jump(Label("_t17"))),
        					Set(7),Set(1),Set()),
        			3 -> Node(3,
        					List(
        						Label("_t16"),
        						Copy("a","4"),
        						Copy("d","5"),
        						Cond_Jump("1",EQ(),"1",Label("_t10"))),
        					Set(5, 4),Set(1),Set()),
        			4 -> Node(4,
        					List(
        						Copy("d","6"),
        						Jump(Label("_t11"))),
        					Set(6),Set(3),Set()),
        			5 -> Node(5,
        						List(
        							Label("_t10"),
        							Copy("c","7")),
        						Set(6),Set(3),Set()),

        			6 -> Node(6,
        					List(
        						Label("_t11"),
        						Copy("b","8")),
        					Set(7),Set(5, 4),Set()),

        			7 -> Node(7,
        					List(
        						Label("_t17"),
        						Bin_Op("a",PLUS(),"b","y"),
        						Bin_Op("c",PLUS(),"d","z"),
        						Bin_Op("i",PLUS(),"1","i")),
        					Set(1),Set(2, 6),Set())
        			)

	"Liveness analysis" should "work" in {
        val cfg = LivenessAnalysis(input)
        DOT.FromCfg(cfg)

        cfg(0).opt_ueVar should equal (Set())
        cfg(0).opt_notKilled should equal (Set("a","b","c","d","y","z"))
        cfg(0).opt_liveOut should equal (Set("i"))

        cfg(1).opt_ueVar should equal (Set())
        cfg(1).opt_notKilled should equal (Set("b","d","i","y","z"))
        cfg(1).opt_liveOut should equal (Set("a","c","i"))

        cfg(2).opt_ueVar should equal (Set())
        cfg(2).opt_notKilled should equal (Set("a","i","y","z"))
        cfg(2).opt_liveOut should equal (Set("a","b","c","d","i"))

        cfg(3).opt_ueVar should equal (Set())
        cfg(3).opt_notKilled should equal (Set("b","c","i","y","z"))
        cfg(3).opt_liveOut should equal (Set("a","c","d","i"))

        cfg(4).opt_ueVar should equal (Set())
        cfg(4).opt_notKilled should equal (Set("a","b","c","i","y","z"))
        cfg(4).opt_liveOut should equal (Set("a","c","d","i"))

        cfg(5).opt_ueVar should equal (Set())
        cfg(5).opt_liveOut should equal (Set("a","c","d","i"))

        cfg(6).opt_ueVar should equal (Set())
        cfg(6).opt_notKilled should equal (Set("a","c","d","i","y","z"))
        cfg(6).opt_liveOut should equal (Set("a","b","c","d","i"))

        cfg(7).opt_ueVar should equal (Set("a","b","c","d","i"))
        cfg(7).opt_notKilled should equal (Set("a","b","c","d"))
        cfg(7).opt_liveOut should equal (Set("i")) 
    }


    "ue vars used before assigned" should "work" in {
		LivenessAnalysis.computeUeVar(Node(7,
        					List(
        						Label("_t17"),
        						Bin_Op("a",PLUS(),"b","y"),
        						Bin_Op("c",PLUS(),"d","z"),
        						Bin_Op("i",PLUS(),"1","i")),
        					Set(),Set(2, 6),Set())) should equal (Set("a","b","c","d","i"))
    }


}