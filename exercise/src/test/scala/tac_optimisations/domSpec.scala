import org.scalatest._
import Matchers._
import at.mCc.tac.optimisations._
import at.mCc.tac._
import at.mCc.ast._
import scala.util._
import scala.io._
import scala.collection.mutable.TreeSet
import scala.collection.immutable.TreeMap

//TODO testing??
class DomSpec extends FlatSpec with Matchers {

    "test_dominators.mC should " should " produce correct domset" in {
        TAC(Source.fromURL(getClass.getResource("test_dominator.mC")).mkString)
          .map(LVN.RemoveLocalDuplicates)
          .map(CFG.FromTacProgram)
          .map(DOM.WithImmedediateFromCfg) should equal (
                                    Success(TreeMap(
                                        1 -> (TreeSet(1),       None),
                                        2 -> (TreeSet(1, 2),    Some(1)),
                                        3 -> (TreeSet(1, 3),    Some(1)),
                                        4 -> (TreeSet(1, 3, 4), Some(3)),
                                        5 -> (TreeSet(1, 3, 5), Some(3)),
                                        6 -> (TreeSet(1, 3, 6), Some(3)),
                                        7 -> (TreeSet(1, 7),    Some(1))
                                    )))
    }

}