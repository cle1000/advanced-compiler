import org.scalatest._
import Matchers._
import at.mCc.tac.optimisations._
import at.mCc.tac.optimisations.types._
import scala.collection.mutable.Set
import at.mCc.tac._
import at.mCc.ast._
import scala.util._
import scala.io._

class CfgSpec extends FlatSpec with Matchers {

    "test_dominators.mC should " should " produce correct controlflowgraph" in {
       h.extract_main_tac(TAC(Source.fromURL(getClass.getResource("test_dominator.mC")).mkString))
          .map(LVN.RemoveLocalDuplicates)
          .map(CFG.FromTacProgram) should equal (
                                    Success(
                                    	Map(
                                    		5 -> Node(5,
                                    					List(Label("_t10"),
                                    						 Bin_Op("$b_0",PLUS(),"5","_t7"),
                                    						 Copy("$e_2","_t7"),
                                    						 Bin_Op("$a_0",PLUS(),"$b_0","_t8"),
                                    						 Copy("$s_0","_t8"),
                                    						 Bin_Op("$e_2",PLUS(),"$f_0","_t9"),
                                    						 Copy("$u_1","_t9")),
                                    					Set(6),
                                    					Set(3)),
                                    		1 -> Node(1,
                                    					List(Copy("$a_0","0"),
                                    						 Copy("$b_0","1"),
                                    						 Copy("$d_0","2"),
                                    						 Copy("$e_0","3"),
                                    						 Bin_Op("$a_0",PLUS(),"$b_0","_t1"),
                                    						 Copy("$m_0","_t1"),
                                    						 Copy("_t2","_t1"),
                                    						 Copy("$n_0","_t2"),
                                    						 Cond_Jump("$a_0",EQ(),"0",Label("_t16"))),
                                    					Set(2, 3),
                                    					Set()),
                                    		6 -> Node(6,
                                    					List(Label("_t11"),
                                    						 Bin_Op("$a_0",PLUS(),"$b_0","_t15"),
                                    						 Copy("$v_0","_t15")),
                                    					Set(7),
                                    					Set(5, 4)),
                                    		2 -> Node(2,
                                    					List(Bin_Op("$e_0",PLUS(),"$d_0","_t18"),
                                    						 Copy("$y_0","_t18"),
                                    						 Jump(Label("_t17"))),
                                    					Set(7),
                                    					Set(1)),
                                    		7 -> Node(7,
                                    					List(Label("_t17"),
                                    						 Bin_Op("$x_0",MUL(),"2","_t19"),
                                    						 Copy("$x_1","_t19"),
                                                             Ret("$x")
                                                             ),
                                    					Set(),
                                    					Set(2, 6)),
                                    		3 -> Node(3,
                                    					List(Label("_t16"),
                                    						 Bin_Op("$a_0",PLUS(),"$b_0","_t4"),
                                    						 Copy("$q_0","_t4"),
                                    						 Bin_Op("$c_0",PLUS(),"$d_0","_t5"),
                                    						 Copy("$r_0","_t5"),
                                    						 Cond_Jump("$q_0",EQ(),"0",Label("_t10"))),
                                    					Set(5, 4),
                                    					Set(1)),
                                    		4 -> Node(4,
                                    					List(Bin_Op("$b_0",PLUS(),"8","_t12"),
                                    						 Copy("$e_1","_t12"),
                                    						 Bin_Op("$c_0",PLUS(),"$d_0","_t13"),
                                    						 Copy("$t_0","_t13"),
                                    						 Bin_Op("$e_1",PLUS(),"$f_0","_t14"),
                                    						 Copy("$u_0","_t14"),
                                    						 Jump(Label("_t11"))),
                                    					Set(6),
                                    					Set(3))))
                                    )

    }

    "for in cfg" should "be valid cfg" in {
      h.tac("int z = 1; for(int i = 1; i < 5; i=i+1){ z = z * i;}").map(CFG.FromTacProgram) should equal (
            Success(Map(
                5 -> Node(5,List(Label("_t5")),Set(),Set(3),Set()),
                1 -> Node(1,List(Copy("$z","1"), Copy("$i","1")),Set(2),Set(),Set()),
                2 -> Node(2,List(Label("_t3"), Cond_Jump("$i",LT(),"5",Label("_t4"))),Set(3, 4),Set(1, 4),Set()),
                3 -> Node(3,List(Jump(Label("_t5"))), Set(5),Set(2),Set()),
                4 -> Node(4,List(Label("_t4"), Bin_Op("$z",MUL(),"$i","_t1"), Copy("$z","_t1"), Bin_Op("$i",PLUS(),"1","_t2"), Copy("$i","_t2"), Jump(Label("_t3"))),Set(2),Set(2),Set())))
      )
    }
}