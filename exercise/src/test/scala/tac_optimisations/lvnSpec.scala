import org.scalatest._
import Matchers._
import at.mCc.tac.optimisations._
import at.mCc.tac._
import at.mCc.ast._
import scala.util._

class LvnSpec extends FlatSpec with Matchers {

 // todo test localvalue basic
    "localvaluenumbering " should " work" in {
        LVN.RemoveLocalDuplicates(List(
            Bin_Op("$x",PLUS(),"$y","_t1"),
            Copy("$a","_t1"),
            Bin_Op("$x",PLUS(),"$y","_t2"),
            Copy("$b","_t2"),
            Copy("$a","42"),
            Bin_Op("$x",PLUS(),"$y","_t3"),
            Copy("$c","_t3")
        )) should equal (List(
                    Bin_Op("$x_0",PLUS(),"$y_0","_t1"),
                    Copy("$a_0","_t1"),
                    Copy("_t2","_t1"),
                    Copy("$b_0","_t2"),
                    Copy("$a_1","42"),
                    Copy("_t3","_t1"),
                    Copy("$c_0","_t3")
        ))
    }


    "localvaluenumbering " should " not work for two blocks" in {
        LVN.RemoveLocalDuplicates(List(
             Bin_Op("$x",PLUS(),"$y","_t1"),
             Copy("$a","_t1"),
             Bin_Op("$x",PLUS(),"$y","_t2"),
             Copy("$b","_t2"),
             Copy("$a","42"),
             Cond_Jump("$a",EQ(),"42",Label("_t5")),
             Jump(Label("_t6")),
             Label("_t5"),
             Bin_Op("$x",PLUS(),"$y","_t4"),
             Copy("$c", "_t4"),
             Label("_t6")
         )) should equal (List(
                 Bin_Op("$x_0",PLUS(),"$y_0","_t1"),
                 Copy("$a_0","_t1"),
                 Copy("_t2","_t1"),
                 Copy("$b_0","_t2"),
                 Copy("$a_1","42"),
                 Cond_Jump("$a_1",EQ(),"42",Label("_t5")),
                 Jump(Label("_t6")),
                 Label("_t5"),
                 Bin_Op("$x_0",PLUS(),"$y_0","_t4"),
                 Copy("$c_0","_t4"),
                 Label("_t6")
             )
         )
    }
}