import org.scalatest._
import Matchers._
import scala.util._
import scala.io._
import at.mCc.compiler._

class IntegrationSpec extends FlatSpec with Matchers {

	"example mod.mC" should "compile" in {
		mCCompiler.Compile(Source.fromURL(getClass.getResource("mod.mC")).mkString) shouldBe a [Success[_]]
	}

	"example assignment.mC" should "compile" in {
		mCCompiler.Compile(Source.fromURL(getClass.getResource("assignment.mC")).mkString) shouldBe a [Success[_]]
	}

	"example ex9.mC" should "compile" in {
		mCCompiler.Compile(Source.fromURL(getClass.getResource("ex9.mC")).mkString) shouldBe a [Success[_]]
	}

	"example test.mC" should "compile" in {
		mCCompiler.Compile(Source.fromURL(getClass.getResource("test.mC")).mkString) shouldBe a [Success[_]]
	}

	"example test1.mC" should "compile" in {
		mCCompiler.Compile(Source.fromURL(getClass.getResource("test1.mC")).mkString) shouldBe a [Success[_]]
	}

	"example test123.mC" should "compile" in {
		mCCompiler.Compile(Source.fromURL(getClass.getResource("test123.mC")).mkString) shouldBe a [Success[_]]
	}

	"example test_syn_error.mC" should "fail" in {
		mCCompiler.Compile(Source.fromURL(getClass.getResource("test_syn_error.mC")).mkString) shouldBe a [Failure[_]]
	}

	"example test_dominator.mC" should "compile" in {
		mCCompiler.Compile(Source.fromURL(getClass.getResource("test_dominator.mC")).mkString) shouldBe a [Success[_]]
	}

	"example scoper.mC" should "compile" in {
		mCCompiler.Compile(Source.fromURL(getClass.getResource("test_dominator.mC")).mkString) shouldBe a [Success[_]]
	}
}