import org.scalatest._
import Matchers._
import at.mCc.parser._
import scala.util._
import scala.io._

class ParserSpec extends FlatSpec with Matchers {

  "parse bin operation expression" should "produce correct pre ast" in {
    h.parse("1+2;") should equal (Success(Vector(
		    									Expr_Statement(
		    												Binary_Operation(
		    													Literal_Expression(Integer("1")),
		    													"+",
		    													Single_Operation(Literal_Expression(Integer("2")))
		    													)
		    												)
		    									)
    										)
    								)
  }

  "parse bin operation expression and ignore whitespace" should "produce correct pre ast" in {
    h.parse("1     +  2           ;") should equal (Success(Vector(
						    									Expr_Statement(
						    												Binary_Operation(
						    													Literal_Expression(Integer("1")),
						    													"+",
						    													Single_Operation(Literal_Expression(Integer("2")))
						    													)
						    												)
						    									)
    														)
						    						)
  }

  "parse assignment expression" should "produce correct pre ast" in {
    h.parse("x=4;") should equal (Success(Vector(
											Expr_Statement(
													Binary_Operation(
															Variable_Expression("x",None),
															"=",
															Single_Operation(Literal_Expression(Integer("4"))))
													)
											)
    									)
    						)
  }

  "parse declaration with assignment" should "produce correct pre ast" in {
    h.parse("int x=4;") should equal (Success(Vector(
    									Declaration_Statement(
    															"int" ,
    															"x",
    															Some(Single_Operation(Literal_Expression(Integer("4"))))
    														)
    									)
    						))
  }


  "parse declaration without assignment" should "produce correct pre ast" in {
    h.parse("int x;") should equal (Success(Vector(
    									Declaration_Statement(
    															"int" ,
    															"x",
    															None
    														)
    									)
    						))
  }

  "parse expression without semicolon" should "fail" in {
    h.parse("1+2") shouldBe a [Failure[_]]
  }

  "parse if" should "produce correct pre ast" in {
    h.parse("if(1=1) x=2; else y=3;") should equal (Success(
    											Vector(
    												  	If_Statement(
    												  		Paren_Expression(
    												  			Binary_Operation(
    												  							Literal_Expression(Integer("1")),
    												  							"=",
    												  							Single_Operation(Literal_Expression(Integer("1")))
    												  							)
    												  		),
    												  		Expr_Statement(
    												  			Binary_Operation(
    												  							Variable_Expression("x",None),
    												  							"=",
    												  							Single_Operation(Literal_Expression(Integer("2")))
    												  							)
    												  		),
    												  		Some(
    												  			Expr_Statement(Binary_Operation(
    												  											Variable_Expression("y",None),
    												  											"=",
    												  											Single_Operation(Literal_Expression(Integer("3")))
    												  											)
    												  			)
    												  		)
    												  	)
    												  )))
  }

  "parse if without else" should "produce correct pre ast" in {
    h.parse("if(1=1) x=2;") should equal (Success(
    											Vector(
    												  	If_Statement(
    												  		Paren_Expression(
    												  			Binary_Operation(
    												  							Literal_Expression(Integer("1")),
    												  							"=",
    												  							Single_Operation(Literal_Expression(Integer("1")))
    												  							)
    												  		),
    												  		Expr_Statement(
    												  			Binary_Operation(
    												  							Variable_Expression("x",None),
    												  							"=",
    												  							Single_Operation(Literal_Expression(Integer("2")))
    												  							)
    												  		),
    												  		None
    												  		
    												  	)
    												  )))
  }

  "parse if with paren" should "produce correct pre ast" in {
    h.parse("if(1=1){x=2;}else y=3;") should equal (Success(
    											Vector(
    												  	If_Statement(
    												  		Paren_Expression(
    												  			Binary_Operation(
    												  							Literal_Expression(Integer("1")),
    												  							"=",
    												  							Single_Operation(Literal_Expression(Integer("1")))
    												  							)
    												  		),Comp_Statement(Vector(
	    												  		Expr_Statement(
	    												  			Binary_Operation(
	    												  							Variable_Expression("x",None),
	    												  							"=",
	    												  							Single_Operation(Literal_Expression(Integer("2")))
	    												  							)
	    												  		)
    												  		)),
    												  		Some(
    												  			Expr_Statement(Binary_Operation(
    												  											Variable_Expression("y",None),
    												  											"=",
    												  											Single_Operation(Literal_Expression(Integer("3")))
    												  											)
    												  			)
    												  		)
    												  	)
    												  )))
  }

  "assign with paren and unary" should "produce correct pre ast" in {
  	h.parse("float x =  - ((1 	+  2 ) )		  ;") should equal (Success(Vector(
  																Declaration_Statement(
	    															"float" ,
	    															"x",
	    															Some(
	    																Single_Operation(
	    																	Unary_Expression(
	    																					"-",
	    																					Single_Operation(Paren_Expression(Single_Operation(Paren_Expression(
										    												Binary_Operation(
										    													Literal_Expression(Integer("1")),
										    													"+",
										    													Single_Operation(Literal_Expression(Integer("2")))
										    													)
										    												)))))		
							    											)
						    											)
	    															)
	    														)

    														)
						    						)

  }

  "while" should "produce correct pre ast" in {
    h.parse("int z = 2; while(!(z > 1000)){ z = z * z;}") should equal (Success(
                                    Vector(
                                        Declaration_Statement("int","z",Some(Single_Operation(Literal_Expression(Integer("2"))))),
                                        While_Statement(Paren_Expression(Single_Operation(
                                                                            Unary_Expression("!",
                                                                                    Single_Operation(Paren_Expression(
                                                                                        Binary_Operation(Variable_Expression("z",None),">",Single_Operation(Literal_Expression(Integer("1000"))))
                                                                                                                    ))))),
                                        Comp_Statement(Vector(
                                                        Expr_Statement(
                                                            Binary_Operation(Variable_Expression("z",None),"=",
                                                                    Binary_Operation(Variable_Expression("z",None),"*",Single_Operation(Variable_Expression("z",None)))))))))))
  }

  "for" should "produce correct pre ast" in {
    h.parse("int z = 1; for(int i = 1; i < 5; i=i+1){ z = z * i;}") should equal (Success(
                                    Vector(
                                        Declaration_Statement("int","z",Some(Single_Operation(Literal_Expression(Integer("1"))))),
                                        For_Statement(
                                            Declaration_Statement("int","i",Some(Single_Operation(Literal_Expression(Integer("1"))))),
                                            Binary_Operation(Variable_Expression("i",None),"<",Single_Operation(Literal_Expression(Integer("5")))),
                                            Binary_Operation(Variable_Expression("i",None),"=",Binary_Operation(Variable_Expression("i",None),"+",Single_Operation(Literal_Expression(Integer("1"))))),

                                            Comp_Statement(Vector(
                                                Expr_Statement(Binary_Operation(Variable_Expression("z",None),"=",Binary_Operation(Variable_Expression("z",None),"*",Single_Operation(Variable_Expression("i",None)))))))))))
  }

  "func and call" should "produce correct pre ast" in {
    mCParser("int print(int a){ return a; } int main(){int a = 3; return print(a); }") should equal (Success(
            Vector(
                Fun_Definition("int","print",Vector(Param("int","a")),
                    Comp_Statement(Vector(
                        Ret_Statement(Single_Operation(Variable_Expression("a", None)))))),
                Fun_Definition("int","main",Vector(),
                    Comp_Statement(Vector(
                        Declaration_Statement("int","a",Some(Single_Operation(Literal_Expression(Integer("3"))))),
                        Ret_Statement(Call_Operation("print",Vector(Single_Operation(Variable_Expression("a",None)))))))))))
  }

  "Array" should "produce correct pre ast" in {
    h.parse("int i = 1; int a[5]; a[0] = 1; a[i+1] = 2;") should equal (Success(
            Vector(
                    Declaration_Statement("int","i",Some(Single_Operation(Literal_Expression(Integer("1"))))),
                    Arr_Declaration_Statement("int","a",Integer("5")),
                    Expr_Statement(Binary_Operation(Variable_Expression("a",Some(Single_Operation(Literal_Expression(Integer("0"))))),"=",Single_Operation(Literal_Expression(Integer("1"))))),
                    Expr_Statement(Binary_Operation(Variable_Expression("a",Some(
                        Binary_Operation(Variable_Expression("i",None),"+",Single_Operation(Literal_Expression(Integer("1")))))),
                    "=",Single_Operation(Literal_Expression(Integer("2"))))))))
  }

}

