
int fib(int n) {
	int ret = 1;
	if(n>2){
		int fib1 = fib(n-1);
		int fib2 = fib(n-2);
		ret = fib1 + fib2;
	}
	return ret;
}	

int main() {
	int i = read_int();
	start_measurement();
	int res = fib(i);
	end_measurement();
	print_int(res);
}
	
