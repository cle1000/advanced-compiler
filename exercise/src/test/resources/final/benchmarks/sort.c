
int sort() {	
	int a[200];
	for(int i = 0; i<200; i=i+1) {
		a[i] = 200-i-21-10-1-1;
	}
	for(int i = 0; i<200-1; i=i+1) {
		for(int j = i+1; j<200; j=j+1) {
			if(a[i] > a[j]) {
				int tmp = a[i];
				a[i] = a[j];
				a[j] = tmp;
			}
		}
	}
	return a[0];
}

int main() {	
	start_measurement();
	int res = sort();
	end_measurement();
	print_int(res);
}
