float dot_product() {
	float a[1000];
	float b[1000];
	float init = 2.0;
	for(int i = 0; i<1000; i=i+1) {
		a[i] = init;
		b[i] = 2.0;
		init = init + 1.0;
	}
	float res = 0.0;
	for(int i = 0; i<1000; i=i+1) {
		res = res + (a[i]*b[i]);
	}
	return res;
}

int main() {	
	start_measurement();
	float res = dot_product();
	end_measurement();
	print_float(res);
}
