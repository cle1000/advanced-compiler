	.globl  main

        .text

main:
	# store old base pointer, save new one
	push    %ebp
	mov     %esp, %ebp
	
	# start measurement
	call start_measurement
	
	# make space on stack for two local integers
	sub     $8, %esp	
	
	# call read_int (for details see hello.s)
        call    read_int
	mov     %eax, 0(%esp)           # move function result to stack position 1
	
	# call another read_int
        call    read_int
	mov     %eax, 4(%esp)           # move function result to stack position 2
	
	# add the two integers
	mov     0(%esp), %eax
	add     4(%esp), %eax
	
	# call print_int to print result
	push    %eax
        call    print_int
	# free argument stack
	add     $4, %esp
		
	# free local stack
	add     $8, %esp
	
	# end measurement
	call end_measurement
	
	# restore base pointer
	pop     %ebp
        ret
