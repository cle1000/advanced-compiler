package at.mCc.compiler

import scala.util._
import sext._
import at.mCc.parser._
import at.mCc.ast._
import at.mCc.tac._
import at.mCc.assembly._
import at.mCc.dot._
import at.mCc.tac.optimisations._
import at.mCc.ast.optimisations._
import at.mCc.bin._
import at.mCc.tac.optimisations.dataflow._
import org.parboiled2.ParseError
import scala.collection.mutable.SortedSet
import scala.collection.mutable.Set

object mCCompiler {

    def Compile(prg: String) = {
        val parser = new mCParser(prg)
        parser.program.run()
        //.map(AST.Test)
        .map(AST.FromParserOutput)
        //.map(CI.FromAST)
        //.map(CI.PrintClassification)
        .map(TAC.FromSyntaxTree)
/*        .map(LVN.RemoveLocalDuplicates)
        .map(CFG.FromTacProgram)
        .map(LivenessAnalysis.FromCfg)
        .map(DOT.FromCfg)*/
        //.map(DOT.StaticCallGraphFromTAC)
        .map(ASSEMBLY.FromTacProgram)
        .map(BINARY.FromASSEMBLY)
        match {
            case Failure(x:ParseError) => Failure(new Exception(parser.formatError(x)))
            case Failure(x) => Failure(x)
            case Success(x) =>  Success(x.toString) //.treeString
        }
    }
}