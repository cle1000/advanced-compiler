package at.mCc.tac
import scala.collection.mutable._

object Identifiers {
    type Scope = List[Int]


	private var variable_counter = 0
    private var variable_counters = HashMap.empty[String, Int]
    private var scope_counter = 0
    private var current_scope = List.empty[Int];
    private var scope_variables = Map.empty[(Scope, String), String]

    def resetIdent() = {
                        variable_counter = 0;
                        variable_counters = HashMap.empty[String, Int];
                        scope_counter = 0;
                        current_scope = List.empty[Int];
                        scope_variables = Map.empty[(Scope, String), String];
                        }

    def getNextUniqueIdentTemp(): String = {
        variable_counter+=1
        "_t" + variable_counter
    }

    def getNextUniqueIdent(name :String): String = {
        if (!name.startsWith("$")) return name

        variable_counters get name match {
            case Some(x) => {
                variable_counters += (name -> (x + 1))
                name + "_" + variable_counters(name)
            }
            case None => {
                variable_counters += (name -> 0)
                name + "_" + 0
            }
        }
    }

    def getLastUniqueIdent(name :String): String = {
        if (!name.startsWith("$")) return name
        variable_counters get name match {
            case Some(x) => {
                name + "_" + x
            }
            case None => {
               getNextUniqueIdent(name)
            }
        }
    }

    def enterScope() = {
        scope_counter += 1;
        current_scope = current_scope :+ scope_counter;
    }

    def exitScope() = {
        var x = current_scope.last;
        current_scope = current_scope.filterNot(elm => elm == x);
    }

    def getNewVariableNameInScope (name : String) = {
        var newVarName = if (!usedInParentScope(name, current_scope)) name else getVarScopeName(name, current_scope)
        scope_variables = scope_variables + ( (current_scope, name) -> newVarName )
        newVarName
    }

    def getFunctionIdentifier(x:String) = "" + x

    def getVariableNameInScope(name : String) = getNameOfVar(name, current_scope)

    private def usedInParentScope(name : String, scope : Scope) : Boolean = scope.take(scope.length-1) match {
        case List() => false
        case xs     => if (scope_variables contains (xs, name)) true else usedInParentScope(name, xs)
    }

    private def getNameOfVar(name : String, scope : Scope) : String =  {
        if (scope.length == 0) /*println("error variable undefined");*/ return name
        if (scope_variables contains (scope, name)) scope_variables((scope, name)) else getNameOfVar(name,  scope.take(scope.length-1))
    }

    private def getVarScopeName(name : String, scope : Scope) = scope.foldLeft("s_"){(acc, e)=> (acc + e + "_")} + name

}