package at.mCc.assembly

import at.mCc.tac._
import at.mCc.tac.types._
import at.mCc.ast._
import at.mCc.assembly.Addresses._
import scala.sys.process._
import java.io._

package object types {
    type ASS_Program = List[String]
}
import types._

object ASSEMBLY {

    def FromTacProgram(tac: TAC_Program): ASS_Program = {
        fromTac(tac)
        // a = a.map{
        //     case x if x.endsWith(":") => x
        //     case x if x.startsWith("ret") => "\t" + x + "\n"
        //     case x if x != "" =>  "\t"+x
        //     case x => x
        // }
    }

    private def is_variable_name(str:String) = str.head == '$' || str.head == '_'
    private def is_arr_name(str: String) = str.head == '%'


    private def fromTac(tac: TAC_Program): (ASS_Program) = {
        var esp = "(%esp)"
        var ebp  ="(%ebp)"
        var eax = "%eax"
        var ebx = "%ebx"
        tac.foldLeft(List(".globl  main", ".text")){(list, t) =>
            list ++ (t match{
                        case StackAlloc(ident,nr) => 
                            allocStackValue(ident,nr)
                            List()
                        case Copy(to,from) =>
                            var t = getOffsetOrValue(to)
                            var f = getOffsetOrValue(from)
                            List(
                                calcArrayIndex(to),
                                (s"mov $f, $eax"),
                                calcArrayIndex(from),
                                (s"mov $eax, $t")
                            )
                        case Un_Op(op, on, to) =>
                            var o = getOffsetOrValue(on)
                            var t = getOffsetOrValue(to)
                            List(
                                calcArrayIndex(on),
                                (s"mov $o %eax"),
                                un_assembly_code(op) + (s" $eax"),
                                calcArrayIndex(to),
                                (s"mov $eax, $t")
                            )
                        case Bin_Op(lhs, op, rhs, to) => {
                            var l = getOffsetOrValue(lhs)
                            var r = getOffsetOrValue(rhs)
                            var d = getOffsetOrValue(to)
                            List(
                                calcArrayIndex(lhs),
                                (s"mov $l, $eax"), 
                                calcArrayIndex(rhs),
                                bin_assembly_code(op) + (s" $r, $eax"),
                                calcArrayIndex(to),
                                (s"mov $eax, $d")
                            )
                        }
                        case Jump(Label(target)) =>
                            List((s"jmp $target"))
                        case Cond_Jump(lhs,op,rhs, Label(target)) =>
                            var l = getOffsetOrValue(lhs)
                            var r = getOffsetOrValue(rhs)
                            var jump = "j" + bin_assembly_code(op)
                            List(
                                calcArrayIndex(lhs),
                                (s"mov $l, $eax"),
                                calcArrayIndex(rhs),
                                (s"mov $r, $ebx"),
                                (s"cmp $ebx, $eax"),
                                (s"$jump $target")
                            )
                        case Label(x) => 
                            List(
                                x + ":"
                            )
                        case Call(Label(x)) =>
                            List(
                                (s"call $x")
                            )
                        case CleanFuncCall(x) =>
                            (if (x == 0) List() else List("add $" + getSizeOfVar(x) + ", %esp")) 
                        case BeginFunc(x) =>
                            enterFunction();
                            List("push %ebp",
                                "mov %esp, %ebp",
                                "sub $" + getSizeOfVar(x) + ", %esp")
                        case EndFunc(x) =>
                            exitFunction()
                            List(
                                "add $" + getSizeOfVar(x) + ", %esp",
                                "pop %ebp",
                                "ret"
                            )
                        case Ret(x) =>
                            var r = getOffsetOrValue(x)
                            List(
                                calcArrayIndex(x),
                                (s"mov $r, $eax")
                            )
                        case Push(x) =>
                            var p = getOffsetOrValue(x)
                            List(
                                calcArrayIndex(x),
                                (s"push $p")
                                )
                        case Pop(pv, pn) =>
                            var v = getOffsetOrValue(pv)
                            var n = getSizeOfVar(pn+1)
                            List(
                                (s"mov $n$ebp, $eax"),
                                calcArrayIndex(pv),
                                (s"mov $eax, $v")
                            )
                    }).filterNot(x => x.isEmpty).map(x => x.split("\n")).flatten
        }
    }

    private def calcArrayIndex(name: String) = if(is_arr_name(name)) "mov " + getOffsetOrValue(name.split("-")(1)) + ", %esi\nneg %esi" else ""

    private def getOffsetOrValue(name:String): String = {
        var ebp  ="(%ebp)"
        if (name == "return") "%eax" 
        else if (is_variable_name(name)) getOffset(name) + ebp
        else if (is_arr_name(name))  getOffset(name.split("-")(0).drop(1)) + "(%ebp,%esi,4)"
        else "$"+name //literal
    }

    private def bin_assembly_code (bin_op : Binary_Operators) = bin_op match {
        case PLUS() => "add"
        case MINUS() => "sub"
        case MUL() => "imul"
        case DIV() => "div"
        case EQ() => "e"
        case NEQ() => "ne"
        case LTE() => "le"
        case GTE() => "ge"
        case LT() => "l"
        case GT() => "g"
        case ASS() => throw new Exception("NOOOPPPP... ASS is no bin assembly instruction")
    }

    private def un_assembly_code (un_op : Unary_Operators) = un_op match {
        case NOT() => "not"
        case NEG() => "neg"
    }

    def apply(x: String) = TAC(x).map(FromTacProgram)
}