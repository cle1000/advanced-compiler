package at.mCc.tac

import at.mCc.ast._
import scala.util._
import at.mCc.tac.Identifiers.{getNextUniqueIdentTemp, getFunctionIdentifier}
import scala.collection.mutable.Set

package object types {
    type TAC_Program = List[TAC]
    type Variables = Set[Variable_Identifier]
    type Variable_Identifier = String
}

import types._
sealed trait TAC {
    def vars : Variables = vars_with_literals.filter(is_variable_name)
    def vars_def : Variables = vars_def_with_literals.filter(is_variable_name)
    def vars_read : Variables = vars_read_with_literals.filter(is_variable_name)
    def vars_def_with_literals: Variables
    def vars_with_literals: Variables
    def vars_read_with_literals: Variables
    def is_variable_name(str:String) = !str.head.isDigit && !str.startsWith("return") && !str.startsWith("%")
}
case class Copy(to: Variable_Identifier,from: Variable_Identifier) extends TAC {
    def vars_with_literals  = Set(to,from)
    def vars_def_with_literals = Set(to)
    def vars_read_with_literals = Set(from)
}
case class Un_Op(op: Unary_Operators, on: Variable_Identifier, to: Variable_Identifier) extends TAC {
    def vars_with_literals  = Set(on,to)
    def vars_def_with_literals = Set(to)
    def vars_read_with_literals = Set(on)
}
case class Bin_Op(lhs: Variable_Identifier, op: Binary_Operators, rhs: Variable_Identifier, to: Variable_Identifier) extends TAC {
    def vars_with_literals  = Set(lhs,rhs,to)
    def vars_def_with_literals = op match {
        case ASS() => Set(lhs)
        case _ => Set(to)
    }
    def vars_read_with_literals = op match {
        case ASS() => Set(rhs)
        case _ => Set(rhs,lhs)
    }
}
case class Jump(target: Label) extends TAC {
    def vars_with_literals  = Set()
    def vars_def_with_literals = Set()
    def vars_read_with_literals = Set()
}
case class Cond_Jump(lhs: Variable_Identifier, op: Binary_Operators, rhs: Variable_Identifier, target:Label) extends TAC {
    def vars_with_literals  = Set()//condition.vars_with_literals
    def vars_def_with_literals = Set()//condition.vars_def_with_literals
    def vars_read_with_literals = Set()//condition.vars_read_with_literals
}
case class Label(name: String) extends TAC {
    def vars_with_literals  = Set()
    def vars_def_with_literals = Set()
    def vars_read_with_literals = Set()
}
case class Pop(to: String, nth_arg: Integer) extends TAC {
    def vars_with_literals  = Set(to)
    def vars_def_with_literals = Set(to)
    def vars_read_with_literals = Set()
}
case class Push(from: String) extends TAC {
    def vars_with_literals  = Set(from)
    def vars_def_with_literals = Set()
    def vars_read_with_literals = Set(from)
}
case class Call(target: Label) extends TAC {
    def vars_with_literals  = Set()
    def vars_def_with_literals = Set()
    def vars_read_with_literals = Set()
}
case class Ret(what: String) extends TAC {
    def vars_with_literals  = Set(what)
    def vars_def_with_literals = Set()
    def vars_read_with_literals = Set(what)
}
case class BeginFunc(var_cout: Integer) extends TAC {
    def vars_with_literals  = Set()
    def vars_def_with_literals = Set()
    def vars_read_with_literals = Set()
}
case class EndFunc(var_cout: Integer) extends TAC {
    def vars_with_literals  = Set()
    def vars_def_with_literals = Set()
    def vars_read_with_literals = Set()
}

// case class PrepareFuncCall() extends TAC{
//     def vars_with_literals  = Set()
//     def vars_def_with_literals = Set()
//     def vars_read_with_literals = Set()
// }
case class StackAlloc(ident: String, elements: Integer) extends TAC{
    def vars_with_literals  = Set((0 to (elements -1)).toList.map(x => "" + ident + "-" + x) :_*)
    def vars_def_with_literals = Set()
    def vars_read_with_literals = Set()
}
case class CleanFuncCall(param_cout: Integer) extends TAC {
    def vars_with_literals  = Set()
    def vars_def_with_literals = Set()
    def vars_read_with_literals = Set()
}

object TAC {

    //def Exec(prg: TAC_Program) = execProgram(prg,0, Map[String,String]())
    
    def FromSyntaxTree(ast: AST): TAC_Program = { Identifiers.resetIdent(); fromAst(ast)._1 }

    def UsedVariablesForStackAlloc(prg: TAC_Program) = prg.foldLeft(Set[Variable_Identifier]())((acc, e) => acc ++ e.vars)

    def GetFunction(name: String)(prg: TAC_Program) = prg.dropWhile(_ != Label(name)).takeWhile({
        case EndFunc(_) => false
        case _ => true
    }).drop(2)

    def apply(x: String) = AST(x).map(FromSyntaxTree)

    private def fromAst(ast: AST): (TAC_Program,String) = ast match {
        case Var(name, index)              => index match {
                                                            case Some(i_expr) => {
                                                                val (code, res) = fromAst(i_expr)
                                                                (code, "%" + name + "-" + res) // name%index
                                                            }
                                                            case None => (List(), "$" + name)
                                                           }
        case Lit(value, t)          => (List(), value)
        case Un_Expr(op, expr)      => {
            val new_v = getNextUniqueIdentTemp()
            val (code, res) = fromAst(expr)
            (code :+
             Un_Op(op,res,new_v), new_v)
        }
        case Bin_Expr(lhs ,op, rhs) => {
            val (code_lhs, res_lhs) = fromAst(lhs)
            val (code_rhs, res_rhs) = fromAst(rhs)
            op match {
                case ASS() => (code_lhs ++
                                 code_rhs :+
                                 Copy(res_lhs, res_rhs), res_lhs)
                case _ => {
                            val new_v = getNextUniqueIdentTemp()
                            (code_lhs ++
                             code_rhs :+
                             Bin_Op(res_lhs, op, res_rhs, new_v), new_v)
                        }
            }
        }
        case If(condition,
                true_part,
                false_part)         => {
            val (code_cond, res_cond) = fromAst(condition)
            val (code_true, res_true) = fromAst(true_part)
            val label_True = Label(getNextUniqueIdentTemp())
            val label_End = Label(getNextUniqueIdentTemp())
            val (code_cond_clean, cond_jump) = getCondAndJump(code_cond, label_True)
            false_part match {
                case None => {
                    (code_cond_clean ++
                     List(
                            cond_jump,
                            Jump(label_End),
                            label_True
                          ) ++ 
                     code_true :+
                     label_End , "")

                }
                case Some(x) =>{
                    val (code_false, res_false) = fromAst(x)
                    (code_cond_clean ++
                     List(cond_jump) ++
                     code_false ++
                     List(
                            Jump(label_End),
                            label_True
                          ) ++ 
                     code_true :+
                     label_End  , "")
                }
            }
        }
        case While(termination,
                loop_part, _)      => {
            val (code_loop, res_loop) = fromAst(loop_part)
            val label_Loop_Begin = Label(getNextUniqueIdentTemp())
            val label_Loop_Run = Label(getNextUniqueIdentTemp())
            val label_Loop_Exit = Label(getNextUniqueIdentTemp())
            var (code_cond, res_cond) = fromAst(termination)
            var (code_cond_clean, cond_jump) = getCondAndJump(code_cond, label_Loop_Run)
                (
                    List(label_Loop_Begin) ++
                    code_cond_clean ++
                    List(cond_jump) ++
                    List(Jump(label_Loop_Exit)) ++
                    List(label_Loop_Run) ++
                    code_loop ++
                    List(Jump(label_Loop_Begin)) ++
                    List(label_Loop_Exit),
                "")
        }
        case Declaration(t,
                        ident,
                        expr)       => expr match {
                            case Some(x) => {
                                val (code, res) = fromAst(x)
                                (code :+ Copy("$" + ident, res), ident)
                            }
                            case None =>  (List(), ident)
                        }
        case Arr_Declaration(t,
                        ident,
                        nr)       => {
                            (List(StackAlloc(ident,nr)), ident + "-0")
                        }
        case Compound(children)     => children.map(fromAst).reduceLeft{ (acc,i) =>
                                            val (l,s) = acc
                                            val (l2,s2) = i
                                            (l ++ l2, s2)
                                        }
        case Fun_Def(t, ident, params, body) => {
            val label_jump = Label(getFunctionIdentifier(ident))
            val (code, res) = fromAst(body)
            //println(UsedVariablesForStackAlloc(code))
            val used_var = UsedVariablesForStackAlloc(code).size
            val begin_func = BeginFunc(used_var)
            var n = 0
            (List(label_jump) ++ List(begin_func) ++
            params.reverse.map({ case (s,t) => n = n+1; Pop("$"+s,n)}) ++
            code ++ List(EndFunc(used_var)), "")
        }
        case Return(expr) => {
                val (code, res) = fromAst(expr)
                (code ++ List(Ret(res)), "return")
            }
        case Fun_Call(ident, args) => (
                    (args.foldLeft(List[TAC]())((acc,expr) => {
                        val (code, res) = fromAst(expr)
                        acc ++ code ++ List(Push(res))
                    }) ++ List(Call(Label(getFunctionIdentifier(ident))),CleanFuncCall(args.length)))
                , "return")
    }

    private def getCondAndJump(code_cond: TAC_Program, label: Label) : (TAC_Program, Cond_Jump) = {
        var jump_condition = code_cond.last match {
            case Bin_Op(lhs, op, rhs, _) => Cond_Jump(lhs, op, rhs, label)
        }
        var code_cond_without_cpm = code_cond.dropRight(1)
        (code_cond_without_cpm, jump_condition)
    }
}