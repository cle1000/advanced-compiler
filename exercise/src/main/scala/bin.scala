package at.mCc.bin

import scala.sys.process._
import java.io._
import at.mCc.assembly.types._

object BINARY {

	def FromASSEMBLY(ass: ASS_Program): Unit = {
		writeAssembly(ass)
		("gcc -o prg ass.s lib.c -m32").!
	}


    private def writeAssembly(assembly: ASS_Program) = {
        val writer = new PrintWriter(new File("ass.s"))
        assembly.map{
            case x => writer.write(x+"\n")
        }
        writer.close()
    }
}