package at.mCc.tac.optimisations.dataflow
import at.mCc.tac._
import at.mCc.tac.optimisations.types._
import at.mCc.tac.optimisations._
import at.mCc.tac.types._
import scala.collection.mutable.Set

class LivenessAnalysis() extends DataFlowAnalysis[Variables] {

	def reComputeLiveOut(n: Node, cfg: ControlFlowGraph): Unit = {
		val out = n.opt_liveOut 
		out.clear
		out ++= n.succ.map((i) => cfg(i).opt_liveIn).foldLeft(Set[Variable_Identifier]())(_ union _)
	}

	def getLiveIn(n: Node): Variables = Set(n.opt_liveIn.toSeq: _*) //important Set(: _*) creates a copy!!!!

	def reComputeLiveIn(n: Node, cfg: ControlFlowGraph): Unit = {
		val in = n.opt_liveIn
		in.clear
		in ++= n.opt_ueVar union (n.opt_liveOut intersect n.opt_notKilled)
	}
	def init(cfg: ControlFlowGraph): Unit = {
		val vars_graph = CFG.UsedVariables(cfg)
		cfg.foreach{ case (n, node) => node.options ++= Set(
															Live(LivenessAnalysis.computeUeVar(node),Set()),
															UeVar(LivenessAnalysis.computeUeVar(node)),
															NotKilled(LivenessAnalysis.computeNotKilled(node,vars_graph))
													)
					}
	}

	def printState(cfg: ControlFlowGraph) = println(cfg.map{ case (n, node) => (n,node.opt_liveIn,node.opt_liveOut)})
	

}

object LivenessAnalysis {
	def FromCfg(cfg : ControlFlowGraph) : ControlFlowGraph  = LivenessAnalysis(cfg)

	def apply() = new LivenessAnalysis()

	def apply(cfg: ControlFlowGraph) = Worklist(cfg: ControlFlowGraph, new LivenessAnalysis())

	def computeNotKilled(b: Node,all_vars:Variables) = all_vars -- b.vars_def

	def computeUeVar(b: Node) = {
		val used_before_defined=Set[Variable_Identifier]()
		val defined=Set[Variable_Identifier]()
		b.prg.foreach((inst)=>{
				used_before_defined ++= (inst.vars_read -- defined)
				defined ++= inst.vars_def			
		})
		used_before_defined
	}
}