package at.mCc.tac.optimisations.dataflow
import at.mCc.tac._
import at.mCc.tac.optimisations.types._
import at.mCc.tac.types._
import scala.collection.mutable.Set

trait DataFlowAnalysis[T] {
	def reComputeLiveOut(n: Node, cfg: ControlFlowGraph): Unit
	def getLiveIn(n: Node): T
	def reComputeLiveIn(n: Node, cfg: ControlFlowGraph): Unit
	def init(cfg: ControlFlowGraph): Unit
	def printState(cfg: ControlFlowGraph): Unit
}

object Worklist {

	def FindFixpoint[T](cfg: ControlFlowGraph, dfa: DataFlowAnalysis[T]) = {
		 def iterate(state: Nodes): ControlFlowGraph = {
		 	if(state.isEmpty) return cfg

		 	val b = state.head
		 	val state_new = state - b
		 	dfa.reComputeLiveOut(b,cfg)
		 	val t = dfa.getLiveIn(b)
		 	dfa.reComputeLiveIn(b,cfg)

		 	// println(state.map((n)=>n.id))
		 	// dfa.printState(cfg)
		 	// println(b.id)

		 	if (dfa.getLiveIn(b) != t) iterate(state_new union b.pred.map((i)=>cfg(i))) else iterate(state_new)
        }
        dfa.init(cfg)
        iterate(Set(cfg.map({case (n,node) => node}).toSeq :_*))
	}

	def apply[T](cfg: ControlFlowGraph, dfa: DataFlowAnalysis[T]) = FindFixpoint(cfg, dfa)

}


