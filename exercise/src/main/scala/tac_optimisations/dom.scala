package at.mCc.tac.optimisations
import scala.collection.mutable.SortedSet
import at.mCc.tac._

object DOM {
    import at.mCc.tac.optimisations.types._

    def FromCfg(cfg: ControlFlowGraph) = {    
        def iterate(state: Dominator_Set): Dominator_Set = {
            val state_new = state.map{ 
                        case (n, dom) => {
                                        val parent_dom = cfg(n).pred.map{ (n) => state(n) }
                                        (n, SortedSet(n) union  (if (parent_dom.isEmpty) SortedSet() else parent_dom.reduceLeft{ (acc,n)=> acc intersect n }))
                            }
                    }
            if (state != state_new) iterate(state_new) else state
        }
        iterate(cfg.map{ case (n,_) => (n,SortedSet(n))})
    }

    def WithImmedediateFromCfg(cfg: ControlFlowGraph) = WithImmedediateFromDomSet(FromCfg(cfg),cfg)

    def WithImmedediateFromDomSet(dom_set: Dominator_Set, cfg: ControlFlowGraph) =  dom_set.map{
                                                            case(n, dom) => {(n, (dom, getIDOM(cfg, dom, n)))}
                                                        }

    private def getIDOM(cfg : ControlFlowGraph, dom: Dominators, node: Int): Option[Int] = cfg(node).pred.toList match {
        case Nil              => None
        case (parent::_)      => if (dom contains parent) Some(parent) else getIDOM(cfg, dom, parent)
    }


}