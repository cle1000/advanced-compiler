package at.mCc.tac.optimisations
import scala.collection.immutable.ListMap
import at.mCc.tac.optimisations.types._
import at.mCc.tac.types._
import at.mCc.tac._

object BB {
	
	def FromTac(prg: TAC_Program) : Basic_Blocks = {
        var i = 1
        var blocks = prg.map((tac) =>{
                            tac match {
                                case Cond_Jump(_,_,_,_) | Jump(_) => {
                                    val x = (tac, i)
                                    i+=1
                                    x
                                }
                                case Label(_) => {
                                    i+=1
                                    (tac, i)
                                }
                                case _ =>  (tac, i)
                            }
                        }).groupBy(_._2)

        i = 0 // reset to avoid gaps in numbering
        ListMap(blocks.toSeq.sortBy(_._1):_*).map {
            case (n, bs) => {
                i += 1
                (i, bs.map ((line) => {
                    line match {
                        case (tac, n) => tac
                    }
                }))
            }
        }
    }
}