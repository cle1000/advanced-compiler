package at.mCc.tac.optimisations

import at.mCc.tac._
import at.mCc.tac.types._
import at.mCc.tac.Identifiers.{getNextUniqueIdent, getLastUniqueIdent}
import at.mCc.ast._

object SSA{

	def FromTAC(prg: TAC_Program) : TAC_Program = {
        Identifiers.resetIdent()
        prg.map((tac) => {
            tac match{
                case Copy(to,from)              => Copy(getNextUniqueIdent(to), getLastUniqueIdent(from))
                case Un_Op(op, on, to)          => Un_Op(op, getLastUniqueIdent(on), getNextUniqueIdent(to))
                case Bin_Op(lhs, op, rhs, to)   =>  op match {
                        case ASS()              => Bin_Op(getLastUniqueIdent(lhs), op, getLastUniqueIdent(rhs), getNextUniqueIdent(to))
                        case _                  => Bin_Op(getLastUniqueIdent(lhs), op, getLastUniqueIdent(rhs), getLastUniqueIdent(to))
                    }
                case Cond_Jump(l,op,r, target)  => Cond_Jump(getLastUniqueIdent(l),op,getLastUniqueIdent(r), target)
                case x                          => x
            }
        })
    }

}