package at.mCc.tac.optimisations
import scala.collection.mutable.HashMap
import at.mCc.tac._
import at.mCc.tac.types._

object LVN {

	def RemoveLocalDuplicates(prg: TAC_Program) : TAC_Program = BB.FromTac(SSA.FromTAC(prg))
																.foldLeft(List.empty[TAC]){
																	(list, block) => list ++ localValueNumberingForBlock(block._2)
																}

	private def localValueNumberingForBlock(prg:TAC_Program) : TAC_Program = {
	    var operations = HashMap.empty[TAC, String]
	    prg.map {
	        case Bin_Op(lhs,op,rhs,to)      => {
	            operations get Bin_Op(lhs, op, rhs, "") match { //TODO COMUTATIVE!!! a+b = b+a
	                case Some(x) => Copy(to, x)
	                case None => {
	                    operations += (Bin_Op(lhs, op, rhs, "") -> to)
	                    Bin_Op(lhs,op,rhs,to)
	                }
	            }
	        }
	        case Un_Op(op, on, to)          =>{
	            operations get Un_Op(op, on, "") match {
	                case Some(x) => Copy(to, x)
	                case None => {
	                    operations += (Un_Op(op, on, "") -> to)
	                    Un_Op(op, on, to)
	                }
	            }
	        }
	        case x => x
	    }
	}	
}
