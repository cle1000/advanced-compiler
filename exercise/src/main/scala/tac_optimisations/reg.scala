/*package at.mCc.tac.optimisations
import at.mCc.tac.optimisations.types._
import at.mCc.tac.types._
import at.mCc.tac._
import graph._
import at.mCc.tac.optimisations.dataflow._
import scala.collection.mutable.Stack._

object REG {

	def FromFunction(prg: TAC_Program, func: String) = FromTacProgram(TAC.GetFunction(func)(prg))
	def FromFunction(func:String)(prg: TAC_Program) = FromTacProgram(TAC.GetFunction(func)(prg))

	def FromTacProgram(func: TAC_Program) = {
		val cfg_live = LivenessAnalysis(CFG.FromTacProgram(func))
		val vars = CFG.UsedVariables(cfg_live)

		val edges = List(("$a","$b"))

		val inf_graph = new Graph[String,String]()//Graph.fromString("[a-c, a-b, a-d, b-d, c-d, a-f, b-f, c-f, d-f]")
		//println(vars)
		vars.foreach(x => inf_graph.addNode(x))
		edges.foreach({case (a,b) => inf_graph.addEdge(a,b,"")})

		val registers = List("EAX","ECX","EDX")
		val n = registers.length

		//println(vars)
		var spill = List[String]()
		//println(inf_graph.colorNodes)
		var r = Set(inf_graph.colorNodes.map(_._2): _ *)
		while(r.size > n){
			val (v, node) = inf_graph.nodes.head
			inf_graph.removeNode(v)
			spill ::= v
			r= Set(inf_graph.colorNodes.map(_._2) : _ *)
		}
		val grr = inf_graph.colorNodes.map({case (a,b) => (a.value, registers(b-1))})
		//println(grr)	
		//println(spill)

		grr
	}


}*/