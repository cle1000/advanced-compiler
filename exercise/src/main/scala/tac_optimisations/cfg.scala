package at.mCc.tac.optimisations
import scala.collection.immutable.ListMap
import scala.collection.mutable.SortedSet
import scala.collection.mutable.Set
import at.mCc.tac._
import at.mCc.tac.types._
import at.mCc.tac.optimisations.types._


//TODO DATATYPE, print
object CFG {

	def FromTacProgram(prg : TAC_Program): ControlFlowGraph  = FromBasicBlocks(BB.FromTac(prg))

    def FromBasicBlocks(blocks: Basic_Blocks): ControlFlowGraph = {
        var blocksKids = blocks.map{
            case (n, prg) => {
                prg.last match {
                    case Jump(target)           => (n, Node(n, prg, getTargetBlocks(blocks,target), Set()))
                    case Cond_Jump(_,_,_, target)   => (n, Node(n, prg, getTargetBlocks(blocks, target) + (n+1), Set()))
                    case _                      => (n, Node(n, prg, if (n == blocks.size) Set()
                                                             else Set(n+1), Set()))
                }
            }
        }
        // set pred
        blocksKids.foreach { case (n, Node(_, _, succ, _, _)) => {
            succ.foreach {  case (n_s) => blocksKids+= (n_s -> blocksKids(n_s).addPred(n)) }
            }
        }
        blocksKids
    }

    def UsedVariables(cfg: ControlFlowGraph) = cfg.foldLeft(Set[Variable_Identifier]())((acc, e) => acc ++ (e match {
                                                                                                                case (n,Node(_,prg,_,_,_)) => TAC.UsedVariablesForStackAlloc(prg)
                                                                                                            }
    ))

    private def getTargetBlocks(blocks: Basic_Blocks, lbl: TAC): Siblings = Set(blocks.filter({ case (n,prg) => prg contains lbl})
                                                                                      .map({ case (n,prg) => n}).toSeq :_*)

}