package at.mCc.tac.optimisations
import scala.collection.mutable.Set
import at.mCc.tac.types._
import at.mCc.tac._

package object types{
	type ControlFlowGraph = Map[Block_Identifier, Node]
	type Siblings = Set[Block_Identifier]

	type Dominator_Set = Map[Block_Identifier, Dominators]
	type Dominators = Set[Block_Identifier]

	type Basic_Blocks = Map[Block_Identifier, Basic_Block] 
	type Basic_Block = TAC_Program

	type Block_Identifier = Int

	type Nodes = Set[Node]

	type Node_Options = Set[Node_Option]

	case class Node(val id: Block_Identifier, val prg:TAC_Program,val succ: Siblings, val pred: Siblings, val options: Node_Options){
		def addPred(n: Block_Identifier) = Node(id, prg, succ,pred+n, options)
		def opt_ueVar :Variables 		= options.collect {case b:UeVar => b}.head.vars
		def opt_notKilled :Variables 	= options.collect {case b:NotKilled => b}.head.vars
		def opt_liveIn :Variables  		= options.collect {case b:Live => b}.head.in
		def opt_liveOut :Variables  	= options.collect {case b:Live => b}.head.out
		def vars :Variables  			= TAC.UsedVariablesForStackAlloc(prg)
		def vars_def :Variables			= prg.foldLeft(Set[Variable_Identifier]())((acc, e) => acc ++ e.vars_def)
	}

	object Node{
		def apply(id: Block_Identifier, prg:TAC_Program, succ: Siblings, pred: Siblings): Node = Node(id, prg, succ,pred, Set())
	}

	sealed trait Node_Option
	case class Live(val in:Variables, val out:Variables) extends Node_Option
	case class UeVar(val vars:Variables) extends Node_Option
	case class NotKilled(val vars:Variables) extends Node_Option

}