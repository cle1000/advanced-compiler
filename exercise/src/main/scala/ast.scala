package at.mCc.ast
import at.mCc.tac.Identifiers.{enterScope, exitScope, getNewVariableNameInScope, getVariableNameInScope}

import at.mCc.parser._

sealed abstract trait Literal_Types
case class INT() extends Literal_Types
case class FLOAT() extends Literal_Types
case class VOID() extends Literal_Types

sealed abstract trait Unary_Operators
case class NOT() extends Unary_Operators
case class NEG() extends Unary_Operators

sealed abstract trait Binary_Operators
case class PLUS() extends Binary_Operators
case class MINUS() extends Binary_Operators
case class MUL() extends Binary_Operators
case class DIV() extends Binary_Operators
case class EQ() extends Binary_Operators
case class NEQ() extends Binary_Operators
case class LTE() extends Binary_Operators
case class GTE() extends Binary_Operators
case class LT() extends Binary_Operators
case class GT() extends Binary_Operators
case class ASS() extends Binary_Operators

sealed abstract trait AST
case class Var(name:String, indexing_expression: Option[AST]) extends AST
case class Lit(value: String, t: Literal_Types) extends AST
case class Un_Expr(op: Unary_Operators, expr: AST) extends AST
case class Bin_Expr(lhs: AST ,op: Binary_Operators, rhs: AST) extends AST
case class If(condition:AST, true_part:AST, false_part: Option[AST]) extends AST
case class While(termination:AST, loop_part:AST, index:Option[Var]) extends AST
case class Declaration(t: Literal_Types, ident: String, expr: Option[AST]) extends AST
case class Arr_Declaration(t: Literal_Types, ident: String, nr_elements: Int) extends AST
case class Compound(children: Seq[AST]) extends AST
case class Fun_Def(t: Literal_Types, ident: String, params: Seq[(String,Literal_Types)], body: AST) extends AST
case class Return(expr:AST) extends AST
case class Fun_Call(ident: String, args: Seq[AST]) extends AST

object AST {

	def FromParserOutput(program: Seq[Fun_Definition]) = Compound(program.map(handleFunDefinitions)) 

	def apply (x: String) = mCParser(x).map(FromParserOutput)

	// def declaredVariables(ast :AST): List[String] = ast match {
	// 	case If(condition, true_part, false_part) => declaredVariables(true_part) ++ (false_part match {
	// 																														case Some(fp) => declaredVariables(fp)
	// 																														case None => List() 
	// 																													})
	// 	case While(termination, loop_part) => declaredVariables(loop_part)
	// 	case Declaration(t, ident, expr) => List(ident)
	// 	case Arr_Declaration(t, ident, nr_elements) => List(0 to (nr_elements -1)).map(x => ident + "-" + x)
	// 	case Compound(children) => children.foldLeft(List[String]())( (acc,i) => acc ++ declaredVariables(i))
	// 	case Fun_Def(t, ident, params, body) => declaredVariables(body);
	// 	case _ => List()
	// }
	

	private def handleFunDefinitions(fun_def: Fun_Definition): AST = fun_def match {
		case Fun_Definition(ret_type, identifier, params, body) => Fun_Def(handleType(ret_type),
																		   identifier,
																		   params.map({ case Param(t,i) =>(i, handleType(t))}),
																		   handleStatement(body))
	}
	
	private def handleStatement(stat: Statement): AST = stat match {
		case If_Statement(condition,
							true_part,
							false_part) => If(handleSingleExpression(condition),
											  handleStatement(true_part),
											  false_part.map(handleStatement))
		case While_Statement(termination,
							loop_part) => While(handleSingleExpression(termination),
											  handleStatement(loop_part), None)
		case For_Statement(initialization,
							termination,
							increment,
							loop_part) => Compound(Vector(
															handleStatement(initialization),
															While(handleExpression(termination),
																Compound(Vector(
																	handleStatement(loop_part),
																	handleExpression(increment)
																)),
																getIndexVar(initialization)
															)
														)
							)
		case Declaration_Statement(type_name,
									identifier,
									assignment_expr) => Declaration(handleType(type_name),
														handelIdentifier(identifier, true),
														assignment_expr.map(handleExpression))
		case Arr_Declaration_Statement(type_name,
										identifier,
										Integer(nr)) => Arr_Declaration(handleType(type_name),
														  handelIdentifier(identifier, true),
														  nr.toInt)
		case Comp_Statement(statements) => {
											enterScope();
											var c = Compound(statements.map(handleStatement));
											exitScope();
											c
											}
		case Expr_Statement(expr) => handleExpression(expr)
		case Ret_Statement(expr) => Return(handleExpression(expr))
	}

	private def handleSingleExpression(sing_expr: Single_Expression): AST = sing_expr match {
		case Unary_Expression(op, expr) 		=> Un_Expr(handleUnOp(op),
															handleExpression(expr))
		case Literal_Expression(lit: Literal) 	=> handleLiteral(lit)
		case Paren_Expression(expr) 			=> handleExpression(expr)
		case Variable_Expression(ident, index) 		=> Var(handelIdentifier(ident, false), index.map(handleExpression))
	}
	
	private def handleExpression(expr: Expression): AST = expr match {
		case Binary_Operation(lhs, op, rhs) => Bin_Expr(handleSingleExpression(lhs),
														handleBinOp(op),
														handleExpression(rhs))
		case Single_Operation(expr) 		=> handleSingleExpression(expr)
		case Call_Operation(ident, args)  	=> Fun_Call(ident, args.map(e => handleExpression(e)))
	}
	
	private def handleBinOp(str: String) = str match {
		case mCParser.PLUS_TOKEN 	=> PLUS()
		case mCParser.MINUS_TOKEN 	=> MINUS()
		case mCParser.MUL_TOKEN 	=> MUL()
		case mCParser.DIV_TOKEN 	=> DIV()
		case mCParser.EQ_TOKEN 		=> EQ()
		case mCParser.NEQ_TOKEN 	=> NEQ()
		case mCParser.LTE_TOKEN 	=> LTE()
		case mCParser.GTE_TOKEN 	=> GTE()
		case mCParser.LT_TOKEN 		=> LT()
		case mCParser.GT_TOKEN 		=> GT()
		case mCParser.ASS_TOKEN 	=> ASS()
		case _ => throw new Exception(str +  " is not a binary operator")
	}

	private def handleLiteral(lit: Literal) = lit match {
		case Integer(x) 	=> Lit(x, INT())
		case Float(x) 		=> Lit(x, FLOAT())
	}

	private def handleUnOp(str: String) = str match {
		case mCParser.NEG_TOKEN 	=> NEG()
		case mCParser.NOT_TOKEN 	=> NOT()
		case _ => throw new Exception(str +  " is not a unary operator")
	}

	private def handleType(str: String) = str match {
		case mCParser.INT_TOKEN 	=> INT()
		case mCParser.FLOAT_TOKEN 	=> FLOAT()
		case mCParser.VOID_TOKEN 	=> VOID()
		case _ => throw new Exception(str +  " is not a valid type")
	}

	private def handelIdentifier(str: String, create: Boolean) = {
		if (create) getNewVariableNameInScope(str) else getVariableNameInScope(str)
	}

	private def getIndexVar(init: Statement) = {
		init match {
			case Declaration_Statement(_, ident,_) => {
				Some(Var(ident,None))
			}
			case _ => None
		}
	}
}
