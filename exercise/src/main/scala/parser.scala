package at.mCc.parser
 
import org.parboiled2._
 
// sealed abstract class Program
// case class Program(statements: Seq[Statement]) extends Program

sealed trait Statement
case class If_Statement(condition: Single_Expression, true_part: Statement, false_part: Option[Statement]) extends Statement

case class While_Statement(termination: Single_Expression, loop_part: Statement) extends Statement
case class For_Statement(initialization: Declaration_Statement, termination: Expression, increment: Expression, loop_part: Statement) extends Statement

case class Declaration_Statement(type_name: String, identifier: String, assignment_expr: Option[Expression]) extends Statement
case class Arr_Declaration_Statement(type_name: String, identifier: String, nr_element: Literal) extends Statement
case class Comp_Statement(statements: Seq[Statement]) extends Statement
case class Expr_Statement(expr: Expression) extends Statement
case class Ret_Statement(expr: Expression) extends Statement


sealed trait Literal
case class Integer(value:String) extends Literal
case class Float(value:String) extends Literal

sealed trait Expression
case class Binary_Operation(lhs: Single_Expression, op: String, rhs: Expression) extends Expression
case class Single_Operation(expr: Single_Expression) extends Expression
case class Call_Operation(ident: String, args: Seq[Expression]) extends Expression

sealed trait Single_Expression
case class Unary_Expression(op: String, expr: Expression) extends Single_Expression
case class Literal_Expression(lit: Literal) extends Single_Expression
case class Paren_Expression(expr: Expression) extends Single_Expression
case class Variable_Expression(identifier: String, indexing_expression: Option[Expression]) extends Single_Expression

case class Param(type_name: String, ident: String)

case class Fun_Definition(ret_type: String, identifier: String, params: Seq[Param], body: Statement)

class mCParser(val input: ParserInput) extends org.parboiled2.Parser {
  import CharPredicate.{Digit, Alpha, AlphaNum}
  import mCParser._

  implicit def wspStr(s: String) = rule { WhiteSpace ~ str(s) ~ WhiteSpace }
  def WhiteSpace = rule { zeroOrMore(WhiteSpaceChar) }
  
  def type_def_or_void = rule { capture(INT_TOKEN | FLOAT_TOKEN | VOID_TOKEN) ~> ((x) => x.trim)}
  def param = rule { type_def ~ identifier ~> Param}
  def params = rule { zeroOrMore(param).separatedBy(",") }
  def fun_def = rule {type_def_or_void ~ identifier ~ "(" ~ params ~ ")" ~ compound_stmt ~> Fun_Definition}

  def type_def = rule { capture(INT_TOKEN | FLOAT_TOKEN) ~> ((x) => x.trim)}

  def expression: Rule1[Expression] = rule { binary_operation | call_expr ~> Call_Operation | single_expr ~> Single_Operation }
  def binary_operation: Rule1[Expression] = rule { (single_expr ~ WhiteSpace ~ bin_operator ~ WhiteSpace ~ expression) ~> Binary_Operation }
  def call_expr = rule { identifier ~ "(" ~ arguments ~ ")"}
  def arguments = rule { zeroOrMore(expression).separatedBy(",") }

  def single_expr : Rule1[Single_Expression] = rule { paren_expr | unary_expr | literal | variable }
  def paren_expr: Rule1[Single_Expression] = rule { "(" ~ expression ~ ")" ~> Paren_Expression }
  def unary_expr: Rule1[Single_Expression] = rule { (un_operator ~ expression) ~> Unary_Expression }
  def literal = rule { (float_lit | int_lit) ~> Literal_Expression }

  def variable = rule { identifier ~ optional("[" ~ expression ~ "]") ~> Variable_Expression }

  def un_operator = rule { capture( NEG_TOKEN | NOT_TOKEN ) ~> ((x) => x.trim) }
  def bin_operator: Rule1[String] = rule { capture(
                                                PLUS_TOKEN |
                                                MINUS_TOKEN |
                                                MUL_TOKEN |
                                                DIV_TOKEN |
                                                EQ_TOKEN |
                                                NEQ_TOKEN |
                                                LTE_TOKEN |
                                                GTE_TOKEN |
                                                LT_TOKEN |
                                                GT_TOKEN |
                                                ASS_TOKEN )  ~> ((x:String) => x.trim) }

  def int_lit = rule { capture(oneOrMore(Digit)) ~> Integer }
  def float_lit = rule { capture(oneOrMore(Digit) ~ "." ~ zeroOrMore(Digit)) ~> Float }
  
  def identifier = rule { capture(alpha ~ zeroOrMore(alphanum)) ~> ((x:String) => x.trim)}
  def alphanum = rule { AlphaNum ++ '_' }
  def alpha = rule { Alpha ++ '_'}

  def statement: Rule1[Statement] = rule { if_stmt | decl_stmt | compound_stmt | expr_stmt | while_stmt | for_stmt | return_stmt | arr_decl_stmt}
  def while_stmt = rule { ("while" ~ paren_expr ~ statement) ~> While_Statement }
  def for_stmt = rule { ("for" ~ "(" ~ decl_stmt ~ expression ~ ";" ~ expression ~ ")" ~ statement) ~> For_Statement }
  def if_stmt = rule { ("if" ~ paren_expr ~ statement ~ optional("else" ~ statement)) ~> If_Statement }
  def decl_stmt = rule { (type_def ~ identifier ~ optional("=" ~ expression) ~ ";") ~> Declaration_Statement}
  def compound_stmt = rule { ("{" ~ oneOrMore(statement) ~ "}") ~> Comp_Statement }
  def expr_stmt = rule { (expression ~ ";" ) ~> Expr_Statement }
  def return_stmt = rule { "return" ~ expression ~ ";" ~> Ret_Statement}

  def arr_decl_stmt = rule { (type_def ~ identifier ~ "[" ~ int_lit ~ "]" ~ ";") ~> Arr_Declaration_Statement}

  def program = rule { zeroOrMore(fun_def) ~ EOI }
}

object mCParser {
  val WhiteSpaceChar = CharPredicate(" \n\r\t\f")
  val QuoteBackslash = CharPredicate("\"\\")
  val QuoteSlashBackSlash = QuoteBackslash ++ "/"

  val INT_TOKEN = "int"
  val FLOAT_TOKEN = "float"
  val VOID_TOKEN = "void"

  val NEG_TOKEN = "-"
  val NOT_TOKEN = "!"

  val PLUS_TOKEN = "+"
  val MINUS_TOKEN = "-" 
  val MUL_TOKEN = "*"
  val DIV_TOKEN = "/"
  val EQ_TOKEN = "=="
  val NEQ_TOKEN = "!="
  val LTE_TOKEN = "<="
  val GTE_TOKEN = ">="
  val LT_TOKEN = "<"
  val GT_TOKEN = ">"
  val ASS_TOKEN = "="

  def apply (x: String) = new mCParser(x).program.run()
}
