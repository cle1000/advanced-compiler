package at.mCc.dot
import java.io._
import scala.util._
import at.mCc.tac.optimisations.types._
import at.mCc.tac.types._
import at.mCc.tac._
import scala.sys.process._

object DOT{

	def FromCfg(cfg: ControlFlowGraph) = {
		val writer = new PrintWriter(new File("graph.dot"))
		writer.write(getStart());
		cfg.map {
            case (ident, n) => {
                n.succ.map {
					case x => {
						writer.write(ident + " -> " + x + ";\n")
					}
                }
                writer.write(ident + " [label=\"\\l")
                writer.write("ID: " + ident + "\\l")
                /*writer.write("UEVAR: " + n.opt_ueVar + "\\l")
                writer.write("NOTKILLED: " + n.opt_notKilled + "\\l")
                writer.write("LIVEOUT: " + n.opt_liveOut + "\\l")*/
                writer.write("|");
                n.prg.map{
                    case x => {
                        writer.write(x+"\\l");
                    }
                }
                writer.write("\"];\n")
            }
        }
        writer.write(getEnd());
		writer.close()
        runDot()
    }

    def StaticCallGraphFromTAC(prg: TAC_Program) = {
        val writer = new PrintWriter(new File("graph.dot"))
        writer.write(getStart());
        var current_func=""
        prg.foreach {
            case Call(Label(ident)) => {
                writer.write(current_func.stripPrefix("#") + " -> " + ident.stripPrefix("#") + ";\n")
                writer.write(ident + " [label=\"\\l")
                writer.write("ID: " + ident + "\\l")
                writer.write("|");
                writer.write("\"];\n")
            }
            case Label(test) => if(test.startsWith("#")) current_func=test 
            case _ => 
        }
        writer.write(getEnd());
        writer.close()
        runDot()
    }

    private def getStart() = "digraph foo { \n node [shape=record,height=.08,fontsize=11]; \n"
    private def getEnd() = "}";

    private def runDot() = {
        ("dot -Tpng ./graph.dot"  #> new File("output.png")).!
        ("rm graph.dot").!
    }
}