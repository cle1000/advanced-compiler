package at.mCc.ast.optimisations
import at.mCc.ast._
import scala.util._
import scala.collection.mutable.Set

object CI {

    type Classification = List[(AST, Set[String])]
    private var indexs = Set.empty[String]

    def FromAST(ast: AST) : Classification =  ast match {
        case If(condition,
                true_part,
                false_part)         => {
                    FromAST(condition) ++
                    FromAST(true_part) ++ 
                    (false_part match {
                        case Some(x) => FromAST(x)
                        case None => List()
                    })
                }
        case Compound(children)     => {
           children.map(FromAST).reduceLeft{(acc, i) => acc ++ i}
        }
        case While(termination,
                loop_part, indexVar)  => indexVar match{
                    case Some(Var(i, _)) => {
                        indexs = indexs + i
                        var lst = FromAST(loop_part)
                        indexs = indexs - i
                        lst
                    }
                    case None => List()
        }
        case Fun_Def(_, _, _, body) => FromAST(body)
        case Fun_Call(ident, args) => {
            var usedVars = args.map(getUsedVars).reduceLeft{(acc,i) => acc ++ i }
            getClassification(Fun_Call(ident,args), usedVars)
        }
        case Arr_Declaration(x1, ident, x2) => {
            var usedVars = Set(ident)
            getClassification(Arr_Declaration(x1, ident, x2), usedVars)
        }
        case Declaration(a, ident, expr) => {
            var usedVars = Set(ident)
            usedVars = usedVars ++ (expr match {
                case Some(x) => getUsedVars(x)
                case None => Set()
            })
            getClassification(Declaration(a, ident, expr), usedVars)
        }

        case Un_Expr(op, expr)      => {
            var usedVars = getUsedVars(expr)
            getClassification(Un_Expr(op, expr), usedVars)
        }
        case Bin_Expr(lhs ,op, rhs) => {
            var usedVars = getUsedVars(lhs)
            usedVars = usedVars ++ getUsedVars(rhs)
            getClassification(Bin_Expr(lhs ,op, rhs), usedVars)
        }
        case _ => List()
    }

    private def getClassification(ast:AST, usedVars: Set[String]) : Classification = {
        if (indexs.size == 0) 
            List()
        else{
            List((ast, indexs.intersect(usedVars)))
        }
    }


    private def getUsedVars(ast : AST) : Set[String] = ast match {
        case Var(name, index) => {
            Set(name) ++
            (index match {
                case Some(x) => getUsedVars(x)
                case _ => Set()
            })
        }
        case Un_Expr(_, expr) => {
            getUsedVars(expr)
        }
        case Bin_Expr(lhs,_,rhs) => {
            getUsedVars(lhs) ++ getUsedVars(rhs)
        }
        case Fun_Call(_, args) => {
            args.map(getUsedVars).reduceLeft{(acc,i) => acc ++ i }
        }
        case _ => {Set()}
    }

    def PrintClassification (cs : Classification) = {
        cs.map((c) => c match{
            case (ast, uVar) => print (ast + ": " + getClassificationName(uVar.size) + "(" + uVar + ")\n")
        })
    }

    private def getClassificationName(size: Integer) ={
        if (size == 0) "ZIV"
        else if (size == 1) "SIV"
        else "MIV"
    }
}