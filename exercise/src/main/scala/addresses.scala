package at.mCc.assembly
import scala.collection.mutable._

object Addresses {
    type FunctionScope = List[Int]
    private var sizeOfElement = 4;

    private var function_counter = 0
    private var current_function = List.empty[Int];
    private var function_addresses = Map.empty[FunctionScope, Map[String,Int]]

    def reset() = {
        function_counter = 0;
        current_function = List.empty[Int];
        function_addresses = Map.empty[FunctionScope, Map[String,Int]]
    }

    def enterFunction() = {
        function_counter += 1;
        current_function = current_function :+ function_counter;
        function_addresses = function_addresses + (current_function -> LinkedHashMap.empty[String,Int])
    }

    def exitFunction() = {
        var x = current_function.last;
        current_function = current_function.filterNot(elm => elm == x);
    }

    def getOffset(name: String) : Integer =  {
        var mapOfAddresses = function_addresses(current_function)
        //var index = mapOfAddresses.indexOf(name)

        mapOfAddresses.get(name) match {
            case Some(size) => (mapOfAddresses.takeWhile(x => x._1 != name).values.sum + 1) * sizeOfElement * -1
            case None => {
                allocStackValue(name, 1)
                (mapOfAddresses.values.sum + 1) * sizeOfElement * -1
            }
        }

    }

    def allocStackValue(name: String, size:Int) = {
        var mapOfAddresses = function_addresses(current_function)

        function_addresses = function_addresses + (current_function -> (mapOfAddresses + (name -> size)))
    }

    def getSizeOfVar(count: Integer) : Integer = count * sizeOfElement
}